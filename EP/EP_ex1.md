<center> <h1> Exercices Epreuves pratiques ( première partie) </h1> </center>

# MOYENNE
# 1 a
Programmer la fonction `moyenne` prenant en paramètre un tableau d'entiers tab (type list) qui renvoie la moyenne de ses éléments si le tableau est non vide et affiche 'erreur' si le tableau est vide.
Exemples :

```python
>>> moyenne([5,3,8])  
5.333333333333333  
>>> moyenne([1,2,3,4,5,6,7,8,9,10])  
5.5  
>>> moyenne([])  
'erreur'  
```

# 1 b
Ecrire une fonction qui prend en paramètre un tableau d'entiers non vide et qui renvoie la moyenne de ces entiers. La fonction est spécifiée ci-après et doit passer les assertions fournies.

```python
def moyenne (tab):
'''
moyenne(list) -> float
Entrée : un tableau non vide d'entiers
Sortie : nombre de type float
Correspondant à la moyenne des valeurs présentes dans le
tableau
'''
assert moyenne([1]) == 1
assert moyenne([1,2,3,4,5,6,7]) == 4
assert moyenne([1,2]) == 1.5
```

# 1 c
Soit le couple (note,coefficient):
* note est un nombre de type flottant (float) compris entre 0 et 20 ;  
*  coefficient est un nombre entier positif.  
Les résultats aux évaluations d'un élève sont regroupés dans une liste composée de couples (note,coefficient). 

Écrire une fonction `moyenne` qui renvoie la moyenne pondérée de cette liste donnée en paramètre.

Par exemple, l’expression moyenne([(15,2),(9,1),(12,3)]) devra renvoyer le résultat du calcul suivant : 
$`\frac{2\times 15 + 1\times  9 + 3\times  12 }{2+1+3} = 12,5`$

# 1 d 
On a relevé les valeurs moyennes annuelles des températures à Paris pour la période allant de 2013 à 2019. Les résultats ont été récupérés sous la forme de deux listes : l’une pour les températures, l’autre pour les années :
```python
t_moy = [14.9, 13.3, 13.1, 12.5, 13.0, 13.6, 13.7]
annees = [2013, 2014, 2015, 2016, 2017, 2
018, 2019]
```

Écrire la fonction mini qui prend en paramètres le tableau `releve` des relevés et le tableau `date` des dates et qui renvoie la plus petite valeur relevée au cours de la période et l’année correspondante.

```python
Exemple :
>>> mini(t_moy, annees)
12.5, 2016
```

# RECHERCHE 1
# 2 a
Programmer la fonction `recherche`, prenant en paramètre un tableau non vide tab (type list) d'entiers et un entier n, et qui renvoie l'indice de la dernière occurrence de l'élément cherché. Si l'élément n'est pas présent, la fonction renvoie la longueur du tableau.

```python
Exemples :  
>>> recherche([5, 3],1)  
2  
>>> recherche([2,4],2)  
0  
>>> recherche([2,3,5,2,4],2)  
3  
```


# 2 c
Écrire une fonction `recherche` qui prend en paramètre un tableau de nombres entiers tab, et qui renvoie la liste (éventuellement vide) des couples d'entiers consécutifs successifs qu'il peut y avoir dans tab.

```python
Exemples :
>>> recherche([1, 4, 3, 5])
[]
>>> recherche([1, 4, 5, 3])
[(4, 5)]
>>> recherche([7, 1, 2, 5, 3, 4])
[(1, 2), (3, 4)]
>>> recherche([5, 1, 2, 3, 8, -5, -4, 7])
[(1, 2), (2, 3), (-5, -4)]
```

# 2 d
Écrire une fonction `RechercheMinMax` qui prend en paramètre un tableau de nombres non triés tab, et qui renvoie la plus petite et la plus grande valeur du tableau sous la forme d’un dictionnaire à deux clés ‘min’ et ‘max’. Les tableaux seront représentés sous forme de liste Python.

```python
Exemples : 
>>> tableau = [0, 1, 4, 2, -2, 9, 3, 1, 7, 1] 
>>> resultat = rechercheMinMax(tableau) 
>>> resultat
{'min': -2, 'max': 9} 
>>> tableau = [] 
>>> resultat = rechercheMinMax(tableau) >>> resultat
{'min': None, 'max': None}
```

# 2 e
Écrire une fonction` maxi` qui prend en paramètre une liste tab de nombres entiers et qui renvoie un couple donnant le plus grand élément de cette liste ainsi que l’indice de la première apparition de ce maximum dans la liste.

```python
Exemple :
>>> maxi([1,5,6,9,1,2,3,7,9,8])
(9,3)
```

# 2 f
Écrire une fonction `recherche` qui prend en paramètres `elt` un nombre entier et `tab` un tableau de nombres entiers, et qui renvoie l’indice de la première occurrence de `elt` dans `tab` si `elt` est dans `tab` et -1 sinon.

```python
Exemples :
>>> recherche(1, [2, 3, 4])
-1
>>> recherche(1, [10, 12, 1, 56])
2
>>> recherche(50, [1, 50, 1])
1
>>> recherche(15, [8, 9, 10, 15])
3
```

# 2 g
Écrire une fonction python appelée `nb_repetitions` qui prend en paramètres un élément `elt` et une liste `tab` et renvoie le nombre de fois où l’élément apparaît dans la liste.

```python
Exemples :
>>> nb_repetitions(5,[2,5,3,5,6,9,5])
3
>>> nb_repetitions('A',[ 'B', 'A', 'B', 'A', 'R'])
2
>>> nb_repetitions(12,[1, '! ',7,21,36,44])
0
```

# 2 h
Écrire une fonction `recherche` qui prend en paramètres `elt` un nombre et `tab` un tableau de nombres, et qui renvoie le tableau des indices de elt dans tab si elt est dans tab et le tableau vide [] sinon.

```python
Exemples :
>>> recherche(3, [3, 2, 1, 3, 2, 1])
[0, 3]
>>> recherche(4, [1, 2, 3])
[]
```

# 2 i ( dichotomie)
Écrire une fonction `recherche` qui prend en paramètres un tableau tab de nombres entiers triés par ordre croissant et un nombre entier n, et qui effectue une recherche dichotomique du nombre entier n dans le tableau non vide tab.  
Cette fonction doit renvoyer un indice correspondant au nombre cherché s’il est dans le tableau, -1 sinon.

```python
Exemples:
>>> recherche([2, 3, 4, 5, 6], 5)
3
>>> recherche([2, 3, 4, 6, 7], 5)
-1
```

# 3 MULTIPLICATION
Programmer la fonction `multiplication`, prenant en paramètres deux nombres entiers n1 et n2, et qui renvoie le produit de ces deux nombres.
Les seules opérations autorisées sont l’addition et la soustraction.

```python
Exemples :
>>> multiplication(3,5)
15
>>> multiplication(-4,-8)
32
>>> multiplication(-2,6)
-12
>>> multiplication(-2,0)
```

# CONVERSION BINAIRE
# 4 a
On modélise la représentation binaire d'un entier non signé par un tableau d'entiers dont les éléments sont 0 ou 1.  
Par exemple, le tableau [1, 0, 1, 0, 0, 1, 1] représente l'écriture binaire de l'entier dont l'écriture décimale est $`2^{6} + 2^{4} + 2^{1} + 2^{0} = 83`$.  
À l'aide d'un parcours séquentiel, écrire la fonction convertir répondant aux spécifications suivantes :  

```python
def convertir(T):
"""
T est un tableau d'entiers, dont les éléments sont 0 ou 1 et
représentant un entier écrit en binaire. Renvoie l'écriture
décimale de l'entier positif dont la représentation binaire
est donnée par le tableau T
"""

Exemple :
>>> convertir([1, 0, 1, 0, 0, 1, 1])
83
>>> convertir([1, 0, 0, 0, 0, 0, 1, 0])
130
```

# 4 b 
Écrire une fonction `conv_bin` qui prend en paramètre un entier positif n et renvoie un couple (b,bit) où :
*  b est une liste d'entiers correspondant à la représentation binaire de n;
*  bit correspond aux nombre de bits qui constituent b.

```python
Exemple :
>>> conv_bin(9)
([1,0,0,1],4)
```

Aide :
* l'opérateur // donne le quotient de la division euclidienne : 5//2 donne 2 ;
* l'opérateur % donne le reste de la division euclidienne : 5%2 donne 1 ;
* append est une méthode qui ajoute un élément à une liste existante :
Soit T=[5,2,4], alors T.append(10) ajoute 10 à la liste T. Ainsi, T devient [5,2,4,10].
* reverse est une méthode qui renverse les éléments d'une liste.
Soit T=[5,2,4,10]. Après T.reverse(), la liste devient [10,4,2,5].

On remarquera qu’on récupère la représentation binaire d’un entier n en partant de la gauche en appliquant successivement les instructions :  
b = n%2  
n = n//2  
répétées autant que nécessaire.  

# 5 ALGO GLOUTON
On s’intéresse au problème du rendu de monnaie. On suppose qu’on dispose d’un nombre infini de billets de 5 euros, de pièces de 2 euros et de pièces de 1 euro.  
Le but est d’écrire une fonction nommée rendu dont le paramètre est un entier positif non nul somme_a_rendre et qui retourne une liste de trois entiers n1, n2 et n3 qui correspondent aux nombres de billets de 5 euros (n1) de pièces de 2 euros (n2) et de pièces de 1 euro (n3) à rendre afin que le total rendu soit égal à somme_a_rendre.  
On utilisera un algorithme glouton : on commencera par rendre le nombre maximal de billets de 5 euros, puis celui des pièces de 2 euros et enfin celui des pièces de 1 euros.  

```python
Exemples :
>>> rendu(13)
[2,1,1]
>>> rendu(64)
[12,2,0]
>>> rendu(89)
[17,2,1]
```

# 6 SUITE FIBONACCI
On s’intéresse à la suite d’entiers définie par $`U_{1} = 1`$, $`U_{2} = 1`$ et, pour tout entier naturel n, par $`U_{n+2} = U_{n+1} + U_{n}`$.  
Elle s’appelle la suite de Fibonnaci.  
Écrire la fonction `fibonacci` qui prend un entier n > 0 et qui renvoie l’élément d’indice n de cette suite.
On utilisera une programmation dynamique (pas de récursivité).

```python
Exemples :
>>> fibonacci(1)
1
>>> fibonacci(2)
1
>>> fibonacci(25)
75025
>>> fibonacci(45)
1134903170
```

# 7 DELTA ENCODING
Le codage par différence (delta encoding en anglais) permet de compresser un tableau de données en indiquant pour chaque donnée, sa différence avec la précédente (plutôt que la donnée elle-même). On se retrouve alors avec un tableau de données assez petites nécessitant moins de place en mémoire. Cette méthode se révèle efficace lorsque les valeurs consécutives sont proches.

Programmer la fonction `delta` qui prend en paramètre un tableau non vide de nombres entiers et qui renvoie un tableau contenant les valeurs entières compressées à l’aide cette technique.

```python
Exemples :
>>> delta([1000, 800, 802, 1000, 1003])
[1000, -200, 2, 198, 3]
>>> delta([42])
42
```

# 8 CONJECTURE DE SYRACUSE
Soit un nombre entier supérieur ou égal à 1 :
- s'il est pair, on le divise par 2 ;
- s’il est impair, on le multiplie par 3 et on ajoute 1.

Puis on recommence ces étapes avec le nombre entier obtenu, jusqu’à ce que l’on obtienne la valeur 1.
On définit ainsi la suite $`(u_{n})`$ par :
- $`u_{0}`$ = k , où k est un entier choisi initialement ;
- $`u_{n+1}`$ = $`u_{n}`$ / 2 si un est pair ;
- $`u_{n+1}`$ = 3×$`u_{n}`$ + 1 si un est impair.

__On admet que, quel que soit l’entier k choisi au départ, la suite finit toujours sur la valeur 1__ 

Écrire une fonction `calcul` prenant en paramètres un entier n strictement positif et qui renvoie la liste des valeurs $`u_{n}`$ , en partant de k et jusqu’à atteindre 1.  

```python
Exemple :
>>> calcul(7)
[7, 22, 11, 34, 17, 52, 26, 13, 40, 20, 10, 5, 16, 8, 4, 2, 1]
```

# RECHERCHE 2(chaine)
# 9 a
Écrire une fonction `recherche` qui prend en paramètres __caractere__, un caractère, et __mot__, une chaîne de caractères, et qui renvoie le nombre d’occurrences de __caractere__ dans __mot__, c’est-à-dire le nombre de fois où __caractere__ apparaît dans __mot__.

```python
Exemples :
>>> recherche('e', "sciences")
2
>>> recherche('i',"mississippi")
4
>>> recherche('a',"mississippi")
0
```

# 9 b 
L’occurrence d’un caractère dans un phrase est le nombre de fois où ce caractère est présent. 
Exemples :
* l’occurrence du caractère ‘o’ dans ‘bonjour’ est 2 ;  
* l’occurrence du caractère ‘b’ dans ‘Bébé’ est 1 ;  
* l’occurrence du caractère ‘B’ dans ‘Bébé’ est 1 ;  
* l’occurrence du caractère ‘ ‘ dans ‘Hello world !’ est 2. 

On cherche les occurrences des caractères dans une phrase. On souhaite stocker ces occurrences dans un dictionnaire dont les clefs seraient les caractères de la phrase et les valeurs l’occurrence de ces caractères.  

 Par exemple :  
 avec la phrase 'Hello world !' le dictionnaire est le suivant :  
 {'H': 1,'e': 1,'l': 3,'o': 2,' ': 2,'w': 1,'r': 1,'d': 1,'!': 1} (l’ordre des clefs n’ayant pas d’importance). 
 
Écrire une fonction `occurence_lettres` avec prenant comme paramètre une variable phrase de type str. Cette fonction doit renvoyer un dictionnaire de type constitué des occurrences des caractères présents dans la phrase. 

# 9 c
On considère des mots à trous : ce sont des chaînes de caractères contenant uniquement des majuscules et des caractères '*'.   
Par exemple 'INFO\*MA\*IQUE', "\*\*\*I\*\*\*E\*\*" et '\*S\*' sont des mots à trous.  

Programmer une fonction correspond qui :
* prend en paramètres deux chaînes de caractères `mot` et `mot_a_trous` où mot_a_trous est un mot à trous comme indiqué ci-dessus,
* renvoie :
    * True si on peut obtenir mot en remplaçant convenablement les caractères '*' de mot_a_trous.
    * False sinon.

```python
Exemples :
>>> correspond('INFORMATIQUE', 'INFO*MA*IQUE')
True
>>> correspond('AUTOMATIQUE', 'INFO*MA*IQUE')
False
```

# 9 d  

Sur le réseau social TipTop, on s’intéresse au nombre de « like » des abonnés. Les données sont stockées dans des dictionnaires où les clés sont les pseudos et les valeurs correspondantes sont les nombres de « like » comme ci-dessous :
{'Bob': 102, 'Ada': 201, 'Alice': 103, 'Tim': 50}  

Écrire une fonction `max_dico` qui :  
* Prend en paramètre un dictionnaire dico non vide dont les clés sont des chaînes de caractères et les valeurs associées sont des entiers ;
*  Renvoie un tuple dont :
    - La première valeur est la clé du dictionnaire associée à la valeur maximale ;
    - La seconde valeur est la première valeur maximale présente dans le dictionnaire.

```python
Exemples :
>>> max_dico({'Bob': 102, 'Ada': 201, 'Alice': 103, 'Tim': 50}) 
('Ada', 201)
>>> max_dico({'Alan': 222, 'Ada': 201, 'Eve': 220, 'Tim': 50})
('Alan', 222)
```

# 9 e

Écrire une fonction `occurrence_max` prenant en paramètres une chaîne de caractères chaine et qui renvoie le caractère le plus fréquent de la chaîne. La chaine ne contient que des lettres en minuscules sans accent.

On pourra s’aider du tableau alphabet=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
et du tableau occurrence de 26 éléments où l’on mettra dans occurrence[i] le nombre d’apparitions de alphabet[i]dans la chaine.  
Puis on calculera l’indice k d’un maximum du tableau occurrence et on affichera alphabet[k].

```python
Exemple :
>>> ch='je suis en terminale et je passe le bac et je souhaite poursuivre des etudes pour devenir expert en informatique'
>>> occurrence_max(ch)
'e'
```
# 10
Pour cet exercice :
*  On appelle « mot » une chaîne de caractères composée avec des caractères choisis parmi les 26 lettres minuscules ou majuscules de l'alphabet,
*  On appelle « phrase » une chaîne de caractères :
    * composée avec un ou plusieurs « mots » séparés entre eux par un seul caractère espace ' ',
    * se finissant :
        * soit par un point '.' qui est alors collé au dernier mot,
        * soit par un point d'exclamation '!' ou d'interrogation '?' qui est alors séparé du dernier mot par un seul caractère espace ' '.

Voici quatre exemples de phrases :
*  'Le point d exclamation est separe !'
*  'Il y a un seul espace entre les mots !'
*  'Le point final est colle au dernier mot.'
*  'Gilbouze macarbi acra cor ed filbuzine ?'

Après avoir remarqué le lien entre le nombre de mots et le nombres de caractères espace dans une phrase, programmer une fonction nombre_de_mots qui prend en paramètre une phrase et renvoie le nombre de mots présents dans cette phrase.

```python
Exemples :
>>> nombre_de_mots('Le point d exclamation est separe !')
6
>>> nombre_de_mots('Il y a un seul espace entre les mots !')
9
```

# 11 XOR
L'opérateur « ou exclusif » entre deux bits renvoie 0 si les deux bits sont égaux et 1 s'ils sont différents :
 0 $`\bigoplus `$ 0 = 0 , 0 $`\bigoplus `$ 1 = 1 , 1 $`\bigoplus `$ 0 = 1 , 1 $`\bigoplus `$ 1 = 0

On représente ici une suite de bits par un tableau contenant des 0 et des 1.
```python
Exemples :
a = [1, 0, 1, 0, 1, 1, 0, 1]
b = [0, 1, 1, 1, 0, 1, 0, 0]
c = [1, 1, 0, 1]
d = [0, 0, 1, 1]
```

Écrire la fonction `xor` qui prend en paramètres deux tableaux de même longueur et qui renvoie un tableau où l’élément situé à position i est le résultat, par l’opérateur « ou exclusif », des éléments à la position i des tableaux passés en paramètres.

En considérant les quatre exemples ci-dessus, cette fonction doit passer les tests suivants :
```python
assert(xor(a, b) == [1, 1, 0, 1, 1, 0, 0, 1])
assert(xor(c, d) == [1, 1, 1, 0])
```

# 12 Chaine
Programmer une fonction `renverse`, prenant en paramètre une chaîne de caractères non vide mot et renvoie une chaîne de caractères en inversant ceux de la chaîne mot.

```python
Exemple :
>>> renverse("informatique")
'euqitamrofni'
```

# 13 Relations
On considère des tables (des tableaux de dictionnaires) qui contiennent des enregistrements relatifs à des animaux hébergés dans un refuge. 

Les attributs des enregistrements sont 'nom', 'espece', 'age', 'enclos'. Voici un exemple d'une telle table :

```python
animaux = [ {'nom':'Medor', 'espece':'chien', 'age':5, 'enclos':2},
{'nom':'Titine', 'espece':'chat', 'age':2, 'enclos':5},
{'nom':'Tom', 'espece':'chat', 'age':7, 'enclos':4},
{'nom':'Belle', 'espece':'chien', 'age':6, 'enclos':3},
{'nom':'Mirza', 'espece':'chat', 'age':6, 'enclos':5}]
```

Programmer une fonction` selection_enclos` qui :
* prend en paramètres :
    * une table table_animaux contenant des enregistrements relatifs à des animaux (comme dans l'exemple ci-dessus),
    * un numéro d'enclos num_enclos ;
*  renvoie une table contenant les enregistrements de table_animaux dont l'attribut 'enclos' est num_enclos.

```python
Exemples avec la table animaux ci-dessus :
>>> selection_enclos(animaux, 5)
[{'nom':'Titine', 'espece':'chat', 'age':2, 'enclos':5},
{'nom':'Mirza', 'espece':'chat', 'age':6, 'enclos':5}]
>>> selection_enclos(animaux, 2)
[{'nom':'Medor', 'espece':'chien', 'age':5, 'enclos':2}]
>>> selection_enclos(animaux, 7)
[]
```

# 14
Programmer la fonction `verifie` qui prend en paramètre un tableau de valeurs numériques non vide et qui renvoie True si ce tableau est trié dans l’ordre croissant, False sinon.

```python
Exemples :
>>> verifie([0, 5, 8, 8, 9])
True
>>> verifie([8, 12, 4])
False
>>> verifie([-1, 4])
True
>>> verifie([5])
True
```

# 15 Tri sélection
Écrire une fonction `tri_selection` qui prend en paramètre une liste `tab` de nombres entiers et qui renvoie le tableau trié par ordre croissant.
On utilisera l’algorithme suivant :

* on recherche le plus petit élément du tableau, et on l'échange avec l'élément d'indice 0 ;
* on recherche le second plus petit élément du tableau, et on l'échange avec l'élément d'indice 1 ;
* on continue de cette façon jusqu'à ce que le tableau soit entièrement trié.

```python
Exemple :
>>> tri_selection([1,52,6,-9,12])
[-9, 1, 6, 12, 52]
```
