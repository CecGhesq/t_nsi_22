<center><h1> PROJET ICEWALKER </h1></center>


# Présentation

Marcher sur la glace est un exercice périlleux :

-   on ne peut pas changer de direction,
-   il est difficile de s'arrêter.

Dans ce projet, nous allons nous intéresser à la programmation d'un jeu dans lequel le joueur incarnera un personnage se déplaçant sur la glace.

Le terrain est :

-   rectangulaire
-   entièrement constitué de cases gelées, à l'exception d'une seule case appelée **case finale**.
    Toutefois un mur peut être placé sur un segment adjacent à deux cases.
-   Entouré de murs.

Notre joueur est aidé dans sa quête par une équipe d'autres personnages pouvant se déplacer de la
même manière sur la grille.

Par convention, le joueur principal sera numéroté 0 et les autres joueur 1, 2, 3, &#x2026;

À chaque étape du jeu, le joueur choisit un personnage et une direction parmi quatre (Nord, Sud, Est, Ouest) pour le personnage choisi. 

Ce dernier se déplace alors dans la direction choisie en ligne droite **jusqu'à rencontrer un obstacle**.
La case finale est considérée comme un obstacle pour les joueurs autres que le principal.

Le jeu se finit lorsque :

-   le joueur **principal** est arrivé sur la case finale ;
-   il abandonne la partie (il n'est parfois plus possible de rejoindre la case finale si une mauvaise direction a été prise)

# Travail à effectuer

## Modélisation du jeu
Modéliser les cases du plateau par une classe `Case` :  

* une case peut posséder ou non des murs dans différentes directions, 

* peut contenir ou non un joueur,   

* peut être ou non la case finale.  

Créez une classe `Plateau` permettant de modéliser le plateau du jeu. Voici les fonctionnalités de 
cette classe : 

* Créer une grille grâce à sa largeur et à sa hauteur qui contiendra toutes les instances de classe `Case` ;

* Tester (prédicat) ajouter (et  supprimer) la case finale ;

*  Ajouter (supprimer) des murs dans une direction ou toutes ;

* Placer des joueurs dans le plateau du jeu ; 

    ```python
    >>> g.set_config(((3,4),(1,5),(6,2)))
    ```
    
* Récupérer la position d'un joueur et de tous les joueurs sous la forme d'un tuple de coordonnées :
    
    ```python
    >>> g.get_config()
    ((3,4),(1,5),(3,2))
    >>> g.get_config_joueur(1)  
    (1,5)
    ```

    
* Déplacer un joueur dans une direction selon les règles du jeu ;

L'implémentation de la méthode `__str__`, qui permet d'afficher le plateau du jeu et la création d' une grille à partir d'un fichier sont faites.

Des exemples de tels fichiers sont fournis .
Par exemple : 

```bash
# dimensions
4,5
# case finale
2,4
# nombre de joueurs
2
# coordonnées joueur principal
0,1
# coordonnées autre(s) joueur(s)
2,2
# murs intérieurs coordonnées, direction
1,1,E
1,2,E
1,3,E
2,0,S
1,1,S
2,3,S
1,3,S
0,0,S
```


Il vous faudra également une classe pour modéliser le jeu  `Icewalker`. Un jeu est paramétré par un plateau de jeu.
La fonctionalité principale de cette classe pourrait être :

* permettre de jouer au jeu de manière interactive.  
![](ex1.png)  
On veut également prendre en compte les points suivants :

### Retours en arrière (undo)

Parfois, on s'aperçoit qu'un mouvement nous a été fatal : on est certain de ne
plus pouvoir atteindre la sortie.

En utilisant une structure de données adaptée, faites en sorte que l'on puisse 
revenir en arrière dans l'historique des coups joués. On doit éventuellement pouvoir 
revenir plusieurs coups en arrière, jusqu'à la configuration initiale.
![](ex2.png) 
On pourra appliquer une pénalité au score pour chaque retour en arrière effectué.

### Faire jouer un humain (résolution <q>à la main</q>)


Réalisez un programme qui permet à un joueur humain de jouer au jeu `IceWalker`. 
Vous pouvez, par exemple, vous inspirez de la trace d'exécution fournie dans l'exemple ci-dessous.

<a id="org817e33d"> </a>



### Attribution des scores

Votre programme, d'une manière ou d'une autre, sera paramétré par 

-   le nom du joueur ;
-   l'emplacement de la grille.

Il aura pour résultat le score du joueur : il s'agit du nombre de coups joués par
le joueur pour amener le joueur principal sur la case finale, avec application d'une pénalité pour chaque retour en arrière.
Plus le score est bas, meilleur il est.


D'après Philippe Marquet, professeur Université Lille
