###################################### IceWalker ######################################
#                                    C Ghesquiere                                     #
#                                    avril 2023                                       #
#######################################################################################


#Importations

from Plateau import *
from Pile_coups import *




class Icewalker:
    def __init__(self,plateau):
        '''initie le jeu avec  une instance de classe PlateauIceWalker le dictionnaire d'adjacence des murs et la pile de coups vide
        :plateau : instance de PlateauIceWalker
        '''
        self.plateau = plateau
        self.murs_jeu = plateau.murs
        self.pile_coups = Pile_coups()
        
    
    def play(self, joueur):
        '''fonction principale du jeu
        : joueur: (str) nom de l'utilisateur du jeu
        '''
        joue = True
        while joue == True :
            print(self.plateau)
            reponse = input ( '{} entrez votre mouvement \'numéro_Joueur,direction\' , \'undo \' ou \'q\' (quit) : '.format(joueur))
            if reponse == 'q' :
                
                pass
            elif reponse == 'undo' :
                pass
            
            elif pass
                # appelle la méthode dplct_joueur de la classe Plateau
                gagne = self.plateau.dplct_joueur(num_Joueur, direction)
                if gagne == True :
                    print('gagné')
                    print('Bravo', joueur, ' vous avez réussi en ', str(self.pile_coups.n_coups), 'coups et ',str(self.pile_coups.nb_depile), 'retour(s) en arrière')
                    joue = False
                    return
            else :
                input ( '{} entrez votre mouvement \'numéro_Joueur,direction\' , \'undo \' ou \'q\' (quit) : '.format(joueur))







##### essais
g= Plateau.from_file("datas/g_essai.txt")
game = Icewalker(g)
game.play("Cec")



