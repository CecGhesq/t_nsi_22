class Pile_coups :
    def __init__(self):
        self.pile = []
        self.n_coups = 0
        self.nb_depile = 0
    
    def empile(self, coup):
        '''empile les tuples (joueur, direction) à chaque coup
        :coup: (tuple) 
        '''
        self.n_coups = self.n_coups + 1
        self.pile.append(coup)
        
    
    def depile(self) :
        '''renvoie le tuple (joueur, direction) et incrémente le compteur de dépilation
        '''
        self.nb_depile = self.nb_depile + 1
        return self.pile.pop()