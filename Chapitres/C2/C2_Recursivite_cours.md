---
title: "Langage et programmation : 2_Récursivité"
subtitle: "cours"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---


|Contenu|Capacités attendues|Commentaires|
|:---|:---|:---|
|Récursivité |Ecrire un programme récursif. Analyser le fonctionnement d'un programme récursif | Des exemples relevant de domaines variés sont à privilégier|

# La récursivité 

__📢 Définition :__
>Un algorithme récursif est un algorithme qui résout un problème en calculant des solutions d’instances plus petites du même problème. L’approche récursive est un des concepts de base en informatique.  
On oppose généralement les algorithmes __récursifs__ aux algorithmes dits __itératifs__ qui s’exécutent sans appeler explicitement l’algorithme lui-même.

Ce concept est très proche de la notion mathématique __récurrence__.

## Exemples  :  

* Print gallery M.C.Escher   ([mise en abime](./img/escher-mise-en-abime.gif))
![escher](./img/escher_print_gallery.jpg)  
* La vache qui rit ([mise en abime](./img/vache-qui-rit-mise-en-abime.gif))
![vachequirit](./img/vache-qui-rit.png)

# Fonctions récursives
__📢 Définition :__
>Une fonction est qualifiée de récursive si elle s’appelle elle-même.

Par exemple, cette fonction qui permet de calculer $`x^{y}`$
 (y étant supposé > 0) :

```python
def puissance(x, y):
   if y <= 0 :                  # Si y<= 0 alors …
      return 1                  # Arrêt de la récursion
   else :                       # Sinon …
      return x * puissance(x, y-1)    # auto appel de la fonction  
print(puissance(3,8))
```
Observons sur Python Tutor le déroulé : [lien vers Python Tutor](https://urlz.fr/fUh0)

On remarque une structure de __pile__ : cette notion sera vue de manière plus approfondie dans un chapitre ultérieur.  

## Eléments caractéristiques

1. **Il faut au moins une situation qui ne consiste pas en un appel récursif**  

 ```python
   if y <= 0 :                 
      return 1
 ```

Cette situation est appelée **situation de terminaison** ou **situation d'arrêt** ou **cas d'arrêt** ou **cas de base**.

2. **Chaque appel récursif doit se faire avec des données qui permettent de se rapprocher d'une situation de terminaison**

```python
    x * puissance(x, y-1)
```

Il faut s'assurer que la situation de terminaison est __atteinte après un nombre fini__ d'appels récursifs.
  
La preuve de terminaison d'un algorithme récursif se fait en identifiant la construction d'une suite strictement décroissante d'entiers positifs ou nuls.


**Mauvaise conception récursive**

Respecter la première règle __ne suffit pas__ :

```python
def puissance(x, y) :
    if y <= 0 :                  
      return 1                
   else :                      
      return x * puissance(x, y+1) 
```  

# Récursivité terminale  

## Définition  

> Un algorithme récursif simple est terminal lorsque l'appel récursif est le dernier calcul effectué pour obtenir le résultat. Il n'y a pas de "calcul en attente". L'avantage est qu'il n'y a rien à mémoriser dans la __pile__.  

Ce n'est pas le cas de la fonction `puissance` précédente.

__Exemple__ : Prédicat de présence d'un caractère dans une chaîne :
Un caractère `c` est présent dans une chaîne `s` non vide, s'il est le premier caractère de s ou s'il est présent dans les autres caractères de s. Il n'est pas présent dans la chaîne vide.

```python
def present(c,s):
    ''' exemple de récursivité terminale
    '''
    if len(s) ==0 :
        return False
    elif c==s[0] :
        return True
    else : 
        return present(c,s[1:])
```

voici l'exécution dans [Python Tutor](https://urlz.fr/fUkr)  

&#x26a0; ne pas oublier le `return` dans le "else" . Pourquoi? 

Il existe __d'autres définitions récursives__ plus riches : des cas récursifs _multiples_ (différents appels selon la parité par exemple) , _double récursion_ ( plusieurs appels en cours de définition) , _imbriquée_, _mutuelle_ etc....  

# Rendre terminal un algorithme récursif  
On utilise un accumulateur, passé en paramètre, pour calculer le résultat au fur et à mesure des appels récursifs.
La valeur de retour du cas de base devient la valeur initiale de l'accumulateur et lors d'un appel récursif, le "calcul en attente" sert à calculer la valeur suivante de l'accumulateur.
Ainsi on obtient :  

```python
def occurrences_term(c,s, acc = 0):
    '''recherche du nombre d'occurrences d'un caractère dans une chaîne
    c: type str : caractère recherché
    s: type str : chaine
    sortie  acc : type int ; nombre d'occurences dans la chaine
    '''
    if s == "":
       return acc  # valeur de retour du cas de base
    elif c == s[0]:
        return occurrences_term(c,s[1:], acc + 1) # caractère présent : accumulateur augmenté ; poursuite de la récursivité
    else:
        return occurrences_term(c,s[1:], acc) # caractère absent : poursuite de la récurrence
```

# Etude de la complexité d'une fonction récursive  

__📢 Rappel  :__

> L’objectif d’un calcul de complexité algorithmique temporelle est de pouvoir comparer l’efficacité d’algorithmes résolvant le même problème. Dans une situation donnée, cela permet donc d’établir lequel des algorithmes disponibles est le plus optimal.  
> La complexité en temps d’un algorithme sera exprimé par une fonction, notée T (pour Time), qui dépend :
>
> * de la taille des données passées en paramètres : plus ces données seront volumineuses, plus il faudra d’opérations élémentaires pour les traiter.
On notera n le nombre de données à traiter.  
> * de la donnée en elle même, de la façon dont sont réparties les différentes valeurs qui la constituent.  

On calculera le plus souvent la complexité dans le pire des cas, car elle est la plus pertinente. Il vaut mieux en effet toujours envisager le pire.

Pour comparer des algorithmes, il n’est pas nécessaire d’utiliser la fonction T, mais seulement l’ordre de grandeur asymptotique, noté O (« grand O »).  

|O|type de complexité|
|:---:|:---:|
|O(1)|constante|
|O(log(n))|logarithmique|
|O(n)|linéaire|
|O($`n^2`$)|quadratique|
|O($`2^n`$)|exponentielle|
|O(n!)|factorielle|

Dans le cas des fonctions récursives, notons  $`T_{n}`$ le nombre d'opérations nécessaires pour évaluer ces fonctions. On cherche alors une relation de récurrence impliquant $`T_{n}`$, puis on résout la relation de récurrence.  

* __Exemple__ : le nombre d'opérations pour le calcul de la puissance d'un nombre.  
$`T_{0}`$ = 1  
$`T_{n+1}`$ = $`T_{n}`$ + 1  
On reconnaît une suite arithmétique, d'où $`T_{n}`$ = n + 1 ; l'ordre de grandeur est donc linéaire (O(n)).  

* Complexités typiques  

|Relation de récurrence|Complexité|
|:---:|:---:|
|$`T_{n} = T_{n-1} + O(1)`$|O(n)|
|$`T_{n} =T_{n-1} + O(n)`$|O($`n^2`$)|
|$`T_{n} =2T_{n-1} + O(1)`$|O($`2^n`$)|
|$`T_{n} = nT_{n-1} et T(1) = 1`$|O(n!)|



__📢 A retenir RECURRENCE :__
> Un calcul peut être décrit à l'aide d'une __définition récursive__. L'avantage de cette technique est que l'implémentation est souvent plus proche de la définition.  
> L'écriture d'une __fonction récursive__ nécessite de distinguer les __cas de base__, pour lesquels on peut donner un résultat facilement, et les __cas récursifs__, qui font appel à la définition en cours.  



Sources : 

* Cours NSI C. Faury Lycée Blaise Pascal à Clermond Ferrand  
* Cours DIU Université de Lille P. Marquet
* NSI Terminale Ellipses T. Balabonski,S. Conchon, JC. Filliatre, K Nguyen  
