######################################### for
def for_1():
    n = int(input(" quelle est votre valeur de n?"))
    resultat = 1
    for _ in range(n) :
        resultat = resultat * 2
    print(resultat)

# for_1()

def for_2():
    n = int(input(" quelle est votre valeur de n?"))
    somme = 0
    for i in range (1, n) :
        print(i, "+ " , end="")
        somme = somme + i
    somme = somme + n
    print(n, "= ", somme)
    print("n*(n+1)//2 vaut : ", n*(n+1)//2)

# for_2()

def for_3(car,chaine) :
    compteur = 0
    for c in chaine :
        if c == car :
            compteur = compteur +1
    return compteur

# comp = for_3("s","la spécialité nsi")
# print(comp)

########################################### while
def while_1(n) :
    n_chiffres = 1
    while n>= 10 :
        n = n // 10
        n_chiffres = n_chiffres +1
    return n_chiffres

# print(while_1(23567))

def while_2(n) :
    assert n> 0,"n doit être strictement positif"
    while n !=1 :
        if n%2 == 0 :
            n = n//2
            print(n)
        else :
            n = n * 3 + 1
            print(n)
# while_2(6)
# while_2(-7)


    
##################################### comparaisons
def triangle() :
    a = int(input(" quelle est la valeur du premier côté?"))
    b = int(input(" quelle est la valeur du deuxième côté?"))
    c = int(input(" quelle est la valeur du troisième côté?"))
    if a+b >= c and b+c >= a and a+c >= b :
        print("ceci est un triangle ",end="")
        if a == b and b == c :
            print("équilatéral")
        elif a == b or b == c or c == a:
            print("isocèle")
        else :
            print("squalène")
    else :
        print(" ceci n'est pas un triangle")
        
# triangle()       
    
#################################### listes

def liste_1a(v, liste):
    compteur = 0
    for elt in liste :
        if elt == v:
            compteur = compteur + 1
    return compteur

def liste_1b(v, liste):
    compteur = 0
    for i in range(len(liste)) :
        if liste[i] == v:
            compteur = compteur + 1
    return compteur

# print(liste_1a(2,[1, 2, 4, 7, 3, 6, 8, 2]))
# print(liste_1b(2,[1, 2, 4, 7, 3, 6, 8, 2]))
import random

def liste_2():
    liste= []
    for _ in range(20):
        valeur = random.randint(1,1000)
        liste.append(valeur)
    print(liste)

# liste_2()

def liste_3(n) :
    return ["*" for _ in range(n)]

#print(liste_3(6))

def liste_4() :
    return [i for i in range(1,21) if i%2 == 0]
# print(liste_4())

#########################################tuples
def tup_1(chaine):
    return tuple(chaine)
#print(tup_1("NSI en terminale"))

def tup_2(t):
    valeur_max = t[0]
    indice = 0
    for i in range(len(t)) :
        if t[i] > valeur_max :
            valeur_max = t[i]
            indice = i
    return (valeur_max,indice)
#print(tup_2((4,7,2,8,4,9,0,1)))

##########################################dictionnaires
gamelin = {"nom":"Gamelin" , "prenom":"Daniel", "lieu" : "Surques"}
bertrand = {"nom":"Bertrand" , "prenom":"Jean-Paul", "lieu" : "Surques"}
cernota = {"nom":"Cernota" , "prenom":"Bruno", "lieu" : "Clerques"}
vanrompu = {"nom":"Van Rompu" , "prenom":"Serge", "lieu" : "Sanghen"}

# print(bertrand.keys())
amis = {"A":gamelin, "B":bertrand, "C" : cernota , "D" : vanrompu}

def dico_1(amis) :
    for dico in amis.values():
        print(dico["prenom"])
#dico_1(amis)

def dico_2(amis) :
    liste_hab=[]
    for dico in amis.values() :
        hab = dico["lieu"]
        if hab not in liste_hab :
            liste_hab.append(hab)
    return liste_hab
print(dico_2(amis))