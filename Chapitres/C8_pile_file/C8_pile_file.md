---
title: "Structure de données : 8_Piles et Files"
subtitle: "cours"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

|Contenu|Capacités attendues|Commentaires|
|---|---|---|
|Listes, piles, files : structures linéaires. dictionnairess, index et clé.|Distinguer des structures par le jeu des méthodes qui les caractérisent. Choisir une structure de données adaptée à la situation à modéliser. Distinguer la recherche d'une valeur dans une liste et dans un dictionnaire|On distingue les modes FIFO ( first in first out) et LIFO ( last in first out) des piles et des files|

# Pile

## Définition 📢 

Une __Pile__ (ou _Stack_) est une structure de données linéaire dans laquelle les éléments sont accessibles selon une discipline __LIFO__ (“Last-In First-Out”) : l’élément inséré en dernier est le premier à sortir. 

![ass](./img/ass.jpg)|  

* Insérer un élément dans la pile est appelé __empiler__ ou pousser (push)
* Supprimer un élément de la pile est appelé __dépiler__ (pop)

L’accès aux objets de la pile se fait grâce à un unique pointeur vers le __sommet__ de la pile, appelé top.  

![pile_depile](./img/pile_depile.jpg)

## Interface  

On considère que la pile S ne contiendra que des éléments de même type.  
Cette structure a une interface proposant au moins quatre opérations :  

|Fonction| description|
|:---|:---|
|creer_pile()--> S | revoie une nouvelle pile|
|est_vide(S) --> bool  |renvoie True si la pile est vide|
|empile(S, e) --> None |empile la valeur e sur la pile S|
|depile(S) -->  |extrait et renvoie la valeur sur le sommet de la pile S|


## Implémentations

### Avec une liste chaînée

La structure de liste chaînée donne une manière élémentaire  de réaliser une pile. Empiler un nouvel élément revient à ajouter une nouvelle cellule en tête de liste, tandis que dépiler un élément revient à supprimer la cellule de tête.  
On va ainsi construire une classe `Pile` défini par un unique attribut `contenu` associé à l'ensemble de la pile, stockés sous la forme d'une liste chaînée.  

```python 
class Pile :
    def __init__(self):
        self.contenu = None
    
    def est_vide(self) :
        return self.contenu is None
    
    def empiler(self, e):
        pass

    def depiler(self):

def creer_pile :
    return Pile()       
```

* Le constructeur défini par la méthode __init__(self), construit une pile vide en définissant contenu comme une liste vide.  
* pour la méthode est_vide(), on teste  si le contenu est une liste vide.  

Pour __empiler__ un nouvel élément on utilise la classe `Cellule` du chapitre précédent :  ( on peut réaliser une importation du module Cellule)

```python
class Cellule :
    '''cellule d'une liste chaînée
    '''
    def __init__( self, v, s):
        self.valeur = v
        self.suivante = s
```

* on crée une nouvelle Cellule qui a pour valeur l'élément e que l'on souhaite empiler
* comme cellule suivante, la première cellule de la liste d'origine (self.contenu donc)

```python
class Cellule :
    '''cellule d'une liste chaînée
    '''
    def __init__( self, v, s):
        self.valeur = v
        self.suivante = s
##### empiler de la classe Pile

def empiler(self, e):
        c =  Cellule(e, self.contenu)
        self.contenu = c
```

Pour récupérer la valeur au sommet de la pile on doit consulter la valeur de la première cellule, si elle existe ( sinon réaliser une levée d'exception). Il faut ensuite retirer la valeur et retrouver la liste d'origine. 
```python
##### depiler de la classe Pile

def depiler(self) :
        if self.est_vide() : # on utilise la méthode !!!!
            raise IndexError("dépiler une liste vide")
        else :
            e = self.contenu.valeur
            self.contenu = self.contenu.suivante
            return e
```
### Avec une structure de type list Python

On peut utiliser le type list facilement avec les méthodes connues comme append() ou pop()

```python
class Pile :
    def __init__(self):
        self.contenu = []
    
    def est_vide(self) :
        return len(self.contenu) == 0
        
    def empiler(self, e):
        self.contenu.append(e)
        
    def depiler(self) :
        if self.est_vide() : # on utilise la méthode !!!!
            raise IndexError("dépiler une liste vide")
        else :
            return self.contenu.pop()
```

# Files

## Définition
Une File (ou Queue) est une structure de données linéaire dans laquelle les éléments sont accessibles selon une discipline FIFO (“First-In First-Out”) : le premier élément inséré dans la liste est le premier à en sortir.

* Insérer un élément dans la file est appelé enfiler (enqueue)
* Supprimer un élément de la file est appelé défiler (dequeue)

![fil](./img/file.jpg)

L’accès aux objets de la file se fait grâce à deux pointeurs, l’un pointant sur l’élément qui a été inséré en premier et l’autre pointant sur celui inséré en dernier.

## L'interface

Celle-ci est similaire à celles des piles :  

|Fonction| description|
|:---|:---|
|creer_file()--> F | renvoie une file vide|
|est_vide(S) --> bool  |renvoie True si la pile est vide|
|enfile(F, e) --> None |ajoute la valeur e à l'ariere de la file|
|defile(F) -->  |retire et renvoie l'élément à l'avant|

## Implémentations

### Avec une liste chaînée mutable  

![fil](./img/lc_file.jpg)  

On doit considérer la version mutable des listes chaînées. En effet l'ajout d'un nouvel élément à l'arrière de la file : la cellule qui était la dernière avant l'ajout possède une cellule suivante.  

La seconde différence est la nécessité d'accéder à la dernière valeur sans parcourir toute la liste à chaque fois. On conserve donc la __dernière valeur dans un attribut__ de la classe `File`.  
La file vide est caractérisée par une tête et une queue vide : on peut consulter ainsi un seul des deux attributs.  

\newpage

```python
class File :
    def __init__(self):
        self.tete = None
        self.queue = None
    
    def est_vide(self) :
        # on peut tester tete : les deux doivent être vides
        return self.queue is None 
```

L'ajout d'un nouvel élément à l'arrière de la file demande de créer une nouvelle cellule qui prend la dernière place et donc n'a pas de cellule suivante. Ne pas oublier le cas particulier où la liste est vide avant.  

```python
    def enfile(self,e) :
        '''on enfile sur la queue de la liste
        '''
        c = Cellule(e , None)
        if self.est_vide() :
            self.tete = c
        else :
            self.queue.suivante = c
        self.queue = c
```

Pour retirer un élément, il faut supprimer la première cellule de la file comme dans une pile. Cependant si la cellule est la dernière il faut redéfinir l'attribut `self.queue` à None.  

```python
    def defile(self) :
        '''on récupère la valeur de la tete mais on l'enlève de la file
        '''
        if self.est_vide() :
            raise IndexError ( "defile une file vide")
        else :
            t = self.tete.valeur
            self.tete = self.tete.suivante
            if self.tete is None :
                self.queue = None
            return t
```

### Réaliser une file avec deux piles

Une réalisation radicalement différente de cette même structure de file consiste à utiliser deux piles, ou directement deux listes chaînées immuables.

![file_2piles](./img/2_piles_file.jpg)

On peut utiliser le modèle du jeu de cartes avec une pioche (sur laquelle on prend une carte face cachée) et une défausse ( disposée face visible ).  
Chacun des deux paquets de cartes est une pile, et ces deux paquets forment ensemble la réserve de cartes : 

* toute carte prise dans la réserve est retirée dans l'une de ces piles ( la pioche)
* toute carte remise dans la réserve est ajoutée à l'autre pile ( la défausse )

Une fois la pioche vide on retourne la défausse pour en faire une nouvelle pioche, laissant à la place une défausse vide.

Une file ainsi réalisée est caractérisée par deux attributs `entree` et `sortie`, le premier contenant une pile dans laquelle on ajoute les nouveaux éléments et le second une pile d'où l'on prend les éléments retirés.  


```python
class File : 
    def __init__(self):
        self.entree = None
        self.sortie = None
    
    def est_vide(self) :
        return self.entree.est_vide() and self.sortie.est_vide()
    
    def ajouter(self, e):
        self.entree.empiler(e)

```

Pour retirer un élément, l'opération est délicate : 

* si la pile de sortie n'est pas vide, il suffit de dépiler son premier élément
* si la pile de sortie est vide, il faut retourner toute la pile d'entrée

```python
    def retirer(self):
        if not self.sortie.est_vide() :
            self.sortie.depile()
```
 
Sources :   

* NSI Terminale Nathan les vrais exos S Pasquet M Leopoldoff  

* Cours NSI C Faury Lycée Blaise Pascal à Clermond Ferrand 