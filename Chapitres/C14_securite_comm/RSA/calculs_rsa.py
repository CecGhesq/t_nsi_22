"""
Implémentation du cryptosystème RSA "from scratch"

@author David SALLE 

"""

# Les librairies utilisées
from random import randint


def etre_premier(n):
    """Détermine si le nombre n est premier ou pas

    Parameters:
        n (int) : nombre à tester

    Returns:
        (boolean) : True si le nombre est premier, False sinon
    """
    # TODO : à vous de jouer !
    pass


def generer_nombre_premier_aleatoirement(taille):
    """Génère un nombre premier aléatoirement d'une certaine taille
    
    Parameters:
        taille (int) : la taille en bits du nombre premier (approximativement)

    Returns:
        (int) : un nombre premier
    """
    # TODO : à vous de jouer !
    pass


def generer_clefs_rsa(taille):
    """Génère la clef publique et la clef publique RSA d'une taille minimum

    Parameters:
        taille (int) : la taille en bits du nombre premier

    Returns:
        (tuple, tuple) : (e,n) la clef publique et (d,n) la clef privée
    """
    # Etape #01 : choisir aléatoirement 2 nombres premiers p et q   
    # TODO : à vous de jouer !

    # Etape #02 : calculer n (module de chiffrement) avec la formule suivante : n = p * q
    # TODO : à vous de jouer !

    # Etape #03 : calculer le produit phi = (p - 1)(q - 1)
    # TODO : à vous de jouer !

    # Etape #04 : choisir e (exposant de chiffrement) tel que :
    #   => 1 < e < phi
    #   => e et phi n'ont pas de facteur commun à part 1 (pgcd(e, phi) = 1)
    # TODO : à vous de jouer !

    # Etape #05 : calculer d (exposant de déchiffrement) tel que  :
    #   => d < phi
    #   => (e * d) mod (p - 1)(q - 1) = 1
    # TODO : à vous de jouer !
    pass


def chiffrer_message(message_clair, clef_publique):
    """Chiffre un message en utilisant la clef publique RSA

    Parameters:
        message_clair (integer) : le code PIN en clair à chiffrer
        clef_publique (tuple) : clef publique RSA (e,n)

    Returns:
        (integer) : le code PIN chiffré
    """
    # Etape #07 : chiffrer le code PIN avec la formule : c = (m^e) mod n
    # /!\ le message m doit être strictement plus petit que n /!\
    # TODO : à vous de jouer !
    pass


def dechiffrer_message(message_chiffre, clef_privee):
    """Déchiffre un message en utilisant la clef privée RSA

    Parameters:
        message_chiffre (integer) : le code PIN à déchiffrer
        clef_privee (tuple) : clef privée RSA (d,n)

    Returns:
        (integer) : le code PIN déchiffré
    """
    # Etape #08 : déchiffrer les données avec la formule : m = (c^d) mod n
    # # /!\ le message c doit être strictement plus petit que n /!\
    # TODO : à vous de jouer !
    pass


if __name__ == "__main__":
    # Message d'accueil
    print("****************")
    print("*** RSA v0.1 ***")
    print("****************")

    """
    # Etape #06 : partager sa clef publique (e,n) et garder secrètement sa clef privée (d,n)
    clef_publique, clef_privee = generer_clefs_rsa(8)
    print(f"Clef (publique) de chiffrement RSA (e,n) : {clef_publique}")
    print(f"Clef (privée) de déchiffrement RSA (d,n) : {clef_privee}")

    # Etape #07 : chiffrer son message à l'aide de la clef publique (e,n)
    message_clair = 1337
    print("Code PIN en clair        =", message_clair)
    message_chiffre = chiffrer_message(message_clair, clef_publique)
    print("Code PIN chiffré         =", message_chiffre) 

    # Etape #08 : dechiffrer le message à l'aide de la clef privée (d,n)
    message_dechiffre = dechiffrer_message(message_chiffre, clef_privee)
    print("Code PIN déchiffré       =", message_dechiffre) 
    """





