---
title: " 14 Sécurisation des communications"
subtitle: "Architecture matérielle : TP_RSA"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---
# Chiffrement asymétrique RSA

Rappel du fonctionnement du chiffrement asymétrique :  

![](./img/cle_pub_priv.jpg)


Le chiffrement RSA utilise les nombres premiers. On peut s'informer sur cette notion très importante en mathématiques et en cryptographie [ici](https://fr.wikipedia.org/wiki/Nombre_premier) ; On peut trouver des listes de nombres premiers jusque 50 000( celle-ci est infinie...) [ici](https://www.nombres-premiers.fr/liste.html)  

## Exemple
Nous allons suivre un exemple du chiffrement asymétrique RSA : 
* __Etape 1__ : création des clefs publique et privée  
1. on choisit aléatoirement deux nombres premiers __p=17__ et __q=19__
2. on calcule le module n = p x q = __17 x 19 = 323__
3. on calcule phi = (p-1)(q-1) = __(17-1)(19-1) = 288__
4. choisir l’exposant e tel que :
    a. e soit premier avec phi (aucun facteur commun sauf 1)  
    b. 1 < e < phi   

--> on choisit un nombre premier inférieur à phi : par exemple e = 257

5. calculer d tel que :  
    a. (e x d) modulo phi = 1  
    b. d < phi  

(257 * d) mod phi = 1 donc en utilisant l'algorithme de Euclide on trouve l'entier de Bezout : d = 65 ( bon amusement pour les élèves faisant Math Expertes!)

On obtient alors :  
__la clef publique (e,n) soit (257,323)__ 
__la clef privée (d,n) soit (65,323)__

* __Etape 2__ : chiffrement du message
RDV A LA GRANDE HORLOGE A SEPT HEURES  

Conversion du message en nombres (ici les codes ASCII en décimal) et découpage en blocs de même taille où chaque valeur est strictement inférieure à n. Pour cet exemple on découpera selon les caractères.


R D V A L A G R A N D E ...   
82 68 86 65 76 65 71 82 65 78 68 69 ...  

Chiffrement de chaque bloc B avec la formule : __C = B^e mod n__  
82 68 86 65 76 65 71 82 65 78 68  
226 102 52 107 247 107 165 226 107 300 103  
|
+—> 82^257 mod 323 = 226

* __Étape 3__ : déchiffrement du message
Déchiffrement de chaque bloc B avec la formule __B = C^d mod n__  
Reconstitution du message en transformant les blocs déchiffrés en caractères ASCII, puis en lettres.  
A noter qu’on peut chiffrer avec la clef privée pour obtenir une signature.  

## En Python  
La librairie pycryptodome propose différents chiffrements symétriques et asymétriques.  
Dans Thonny, en ligne de commande, installer le module :

```bash
pip install pycryptodome
```

Puis dans Thonny :  

```python
# Librairies utilisées
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES, PKCS1_OAEP
# Génération des clefs RSA publique et privée sur 1024
clefs_rsa = RSA.generate(1024)
clef_publique = clefs_rsa.publickey()
print(f"clef_publique : (e={clef_publique.e}, n={clef_publique.n})")
print(f"clef_privée : (d={clefs_rsa.d}, n={clef_publique.n})")
# Exportation au format PEM dans des fichiers
clef_privee_pem = clefs_rsa.exportKey()
fichier = open("clef_privee.pem", "wb")
fichier.write(clef_privee_pem)
fichier.close()
clef_publique_pem = clef_publique.exportKey()
fichier = open("clef_publique.pem", "wb")
fichier.write(clef_publique_pem)
fichier.close()
```

Exemple de chiffrement RSA en utilisant la clef publique :

```python
# Lecture de la clef publique depuis le fichier
clef_publique = RSA.import_key(open("clef_publique.pem").read())
# Les messages
message_en_clair = "RDV 13H37 AU VIEUX CHENE".encode("utf-8")
message_chiffre = None
# Utilisation de la clef publique pour chiffrer le message
chiffreur_rsa = PKCS1_OAEP.new(clef_publique)
message_chiffre = chiffreur_rsa.encrypt(message_en_clair)
print("message_en_clair => ", message_en_clair)
print("message_chiffre => ", message_chiffre)
```

Exemple de déchiffrement RSA en utilisant la clef privée :

```python
# Lecture de la clef privée depuis un fichier
clef_privee = RSA.import_key(open("clef_privee.pem").read())
# Déchiffrement du message et affichage
dechiffreur_rsa = PKCS1_OAEP.new(clef_privee)
message_dechiffre = dechiffreur_rsa.decrypt(message_chiffre)
print("message_dechiffre => ", message_dechiffre)
```

# Application  
L’objectif de cet exercice est  d’implémenter le cryptosystème RSA  en Python.  

_Vous venez de recevoir ce carton d'invitation_ :  

![](crypto_party_invit.jpg)  

* Télécharger le fichier [calculs_rsa.py](calculs_rsa.py) ; 

* Compléter le code de la fonction dechiffrer_message() ; 

* Testez votre fonction pour déchiffrer le code d’accès du carton d’invitation sachant que votre clef privée est : ( d = 10981, 43931)

* A votre tour vous souhaitez organiser une crypto-party !

➜ Complétez les autres fonctions du script : chiffrer_message(),
generer_clefs_rsa()...  

➜ En binôme testez l’envoi d’un code PIN (nombre de 0000 à 9999) en
transmettant oralement les données : le code PIN chiffré par la clé publique qu'il vous a donné. 
