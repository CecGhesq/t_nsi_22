#AES


# Librairies utilisées
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad
# Les messages
message_en_clair = "RDV 13H37 AU VIEUX CHENE".encode("utf-8")
message_chiffre = None
# La clef privée (longueur 16 octets, la taille des blocs)
clef_privee = "AZERTY0123456789".encode("utf-8")
# Création d'un chiffreur AES
chiffreur = AES.new(clef_privee, AES.MODE_ECB)
# Chiffrement du message et affichage
message_chiffre = chiffreur.encrypt(pad(message_en_clair, 16))
print(message_chiffre)

# Librairies utilisées
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad
# Les messages
message_chiffre =b'\xba\xdd\x07\xa3\xd2n\x8a\x0b&6\xfe\xae\x1e\x80\x9c\xa9J\x9e\xcb \x91\xfe\x03W{\xf5\xaeY\xb1<D\n'
# La clef privée (longueur 8 octets, la taille des blocs)
clef_privee = "AZERTY0123456789".encode("utf-8")
# Déchiffrement du message et affichage
dechiffreur = AES.new(clef_privee, AES.MODE_ECB)
message_dechiffre=unpad(dechiffreur.decrypt(message_chiffre), 16)
print("message_dechiffre => ", message_dechiffre)