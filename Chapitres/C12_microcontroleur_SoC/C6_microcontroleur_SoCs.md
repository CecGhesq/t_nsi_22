---
title: "Architecture matérielle : 12_Microcontrôleur et SoC"
subtitle: "cours"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---
|Contenus|Capacités attendues|Commentaires|
|:---:|:---:|:---:|
|Composants intégrés d'un syst-me sur puce|Identifier les principaux composants sur un schéma de circuit et les avantages de leur intégration en termes de vitesse et de consommation|Le circuit d'un téléphone peut être pris comme un exemple : microprocesseurs, mémoires locales, interfaces radio et filaires, gestion d'énergie, contrôleurs vidéo, accélérateur graphique, réseaux sur puce, etc.|



Retour vers l'histoire de l'architecture des ordinateurs : https://www.lumni.fr/video/une-histoire-de-l-architecture-des-ordinateurs

En 1975, __Gordon E. Moore__, cofondateur de la société Intel a énoncé la conjecture __du doublement tous les deux ans du nombre de transistors utilisés pour la fabrication d’un circuit intégré__. Cette prédiction , appelée aussi __loi de Moore__ s'est révélée incroyablement juste.  
Aujourd’hui on est passé de 2000 transistors à quelques milliards pour les derniers microprocesseurs grâce notamment à la réduction de la taille des transistors (7 nanomètres).  

|||
|:---:|:---:|
|![intel](./img/intel4004.jpg)|![amd](./img/AMD.jpg)|

De plus grâce aux principes de réalisation des puces électroniques, on a pu réduire les valeurs des tensions nécessaires pour les faire commuter, leur consommation énergétique mais aussi leur fréquence de travail.

# Architecture de Von Neumann, rappels de 1ere NSI  

## Architecture von Neuman
Un système microprogrammé est basé autour du processeur, ou microprocesseur, chargé d’effectuer des opérations (calculs, transfert de données, tests, etc...) et de la mémoire permettant le stockage des données nécessaires au programme.  

🎓 On a acquis :  
La première innovation est la *séparation nette* entre l’**unité de commande ou de contrôle**, qui organise le flot de séquencement des instructions(= chef d'orchestre), et l’**unité arithmétique**, chargée de l’exécution proprement dite de ces instructions.  
La seconde innovation, la plus fondamentale, est l’idée du **programme enregistré** : les instructions, au lieu d’être codées sur un support externe (ruban, cartes, tableau de connexions), sont enregistrées dans la mémoire selon un codage conventionnel. Un compteur ordinal contient l’adresse de l’instruction en cours d’exécution ; il est automatiquement incrémenté après exécution de l’instruction, et explicitement modifié par les instructions de branchement.
L’architecture des ordinateurs à programme enregistré comporte les éléments suivants :

* Une unité centrale chargée d’exécuter les instructions d’un programme ;
* Une mémoire centrale qui contient les instructions d’un programme ;
* Des unités d’entrées-sorties prenant en charge l’échange d’informations entre le couple unité centrale-mémoire centrale et l’extérieur de l’ordinateur, via des périphériques tels que le clavier, la souris, l’écran, etc.

![von Neuman](./img/modele-originel2.gif)

On distingue deux types de mémoires associées directement au microprocesseur :  
* la mémoire volatile (RAM) permettant le stockage et la manipulation rapide de données,
* la mémoire non volatile (ROM Read-Only Memory, EEPROM Electrically-Erasable Programmable Read Only Memory, FLASH ...) nécessaire au stockage de données pérennes c’est-à-dire de données que le microprocesseur retrouvera même après une coupure d’alimentation.

Les échanges entre le microprocesseur (CPU : Central Processing Unit) et les différents circuits associés à celui-ci sont basés autour de l’utilisation de bus de communication :  

* le bus d’adresses pour la sélection de la case mémoire
sélectionnée,
* le bus de données pour le transfert de données binaires,
* et un bus de contrôle permettant l’organisation des échanges
entre les différents circuits.  

Le travail d’un microprocesseur peut se résumer de la façon suivante :

* il charge en mémoire une donnée et ces opérandes,
* il décode l’instruction correspondante à ces données,
* il effectue le calcul correspondant et stocke éventuellement le résultat de son calcul en mémoire,
* puis, il passe à l’instruction suivante en pointant sur l’adresse de celle-ci
Pour son travail, il est est aidé de mémoires internes possède appelées registres (PC, A, B, X, etc.).

\newpage
## Du transistor au microprocesseur

Fonctionnement des transistors électroniques NPN et PNP :
http://www.youtube.com/watch?v=Uqvk7x6nmfg&t=1m17s

Fonctionnement des portes logiques à partir d’un assemblage de transistors :
https://www.youtube.com/watch?v=L_EFljkLz_M&t=1m4s

Fonctionnement d’un composant calculateur (additionneur) à partir de portes logiques donc de transistors :
https://www.youtube.com/watch?v=L_EFljkLz_M&t=5m02s

Fabrication d'un microprocesseur en images :  
https://www.tomshardware.fr/diapo-la-fabrication-dun-processeur-expliquee-en-images/
https://www.irif.fr/~carton/Enseignement/Architecture/Cours/Production/  

Fabrication CPU :  
en 2012 :  
https://www.youtube.com/watch?v=SsL78GVCx4E&ab_channel=younessteven  
en 2022  
 https://www.youtube.com/watch?v=1eL8Inu8Eu0&ab_channel=Azellus

# Microcontrôleurs  

Avec la miniaturisation, vint la nécessité d'embarquer l'informatique dans des systèmes mobiles. On trouve ainsi des systèmes embarqués dans les voitures, les avions ou les systèmes d'alarme...

Chacun de ces systèmes a une tâche bien spécifique à réaliser une fois créés et le système ne nécessite donc pas d'avoir la polyvalence d'un 'vrai' ordinateur muni d'un microprocesseur.

Voici donc la liste des différences entre les microprocesseurs et les microcontrôleurs :  

>📢 A RETENIR : Les microcontrôleurs sont beaucoup moins polyvalents mais intègrent directement dans leurs puces un tas de fonctionnalité normalement externes aux microprocesseurs.  
Ils gagnent ainsi en autonomie, notamment énergétique. Cela leur permet de consommer moins d'énergie et donc de pouvoir fonctionner à partir de batteries de petite taille.

>Les microcontrôleurs diposent __de deux zones différentes__ pour le programme et les données. Sur la plupart d'entre eux, on trouve :  
> * Une zone mémoire indépendante et non volatile contenant ce qu'on nomme le firmware, le programme de base permettant le fonctionnement du composant. Il s'agit en quelque sorte d'une sorte de système d'exploitation en version basique. Il est installé de base par le constructeur, en usine mais peut éventuellement être modifié, mais souvent de façon assez technique. Cela évite notamment les manipulations accidentelles.
> * Une zone mémoire non volatile dans laquelle on peut téléverser le programme qu'on fonctionner. A chaque redémarrage de microcontrôleur, ce programme va donc se relancer automatiquement. On peut donc programmer le processeur mais on doit utiliser une autre machine (un 'vrai' ordinateur) pour créer le programme et le téléverser dans le microcontrôleur.
> * Une zone mémoire volatile permettant de stocker les données nécessaires au fonctionnement du programme. Cette mémoire s'efface donc à chaque fois qu'on éteint ou redémarrage le système.

Certains microcontrôleurs profitent de la séparation des mémoires de programmes et de données pour implémenter une architecture dite de Harvard. On accède ainsi aux mémoires programme et données à __travers deux bus distincts__ : cela permet de gagner du temps en transférant simultanément les instructions et les données.  

![Harvard](./img/Architecture_Harvard.png)  

Ces microcontrôleurs jouent donc un rôle très important dans l'informatique embarqué et ont principalement pour but :  

* de recueillir les informations fournis par des capteurs auxquels ils sont reliés
* de commander les actionneurs auxquels ils sont reliés

En conséquence, les microcontrôleurs sont optimisés pour les gestions des entrées/sorties avec l'extérieur et disposent souvent de plus de registres dédiés à cette communication qu'un microprocesseur. C'est pour cela qu'ils disposent également la plupart du temps de broches permettent de brancher le matériel électronique ou d'autres cartes.  

|Carte Arduino Uno|Carte Micro:bit|
|:---:|:---:|
|![arduino](./img/Arduino.jpg)|   ![microbit](./img/Microbit.jpg)|

En conclusion, les trois grandes différences avec un microprocesseurs sont :

* La moindre consommation et le fonctionnement autonome facilité des microcontrôleurs
* La spécialisation des microcontrôleurs dans la gestion des entrées / sorties avec des capteurs et actionneurs
* La moins grande polyvalence du microcontrôleur

\newpage

# Système sur puce (SoC)

## Définition  
A l'heure actuelle, les systèmes informatiques à l'intérieur des smartphones, des tablettes, des montres connectés ou des voitures sont encore plus miniaturisés.

On parle de système sur puce, appelé _System on Chip_ : la puce ne constitue plus le processeur, mais __l'ensemble des composants__ d'un ordinateur classique. On retrouve l'architecture Von Neuman.  

![](./img/SoC.jpg)

Dans cette petite puce, on trouve donc :

* Le processeur
* La mémoire vive
* Les processeurs esclaves (le GPU (processeur graphique) , la carte son, la carte de chiffrement...)
* Beaucoup de capteurs, d'actionneurs ou de dispositifs de communication : cartes réseaux et antennes Wifi, Bluetooth, radio cartes et antenne GPS capteurs de type accéléromètre, magnétomètre...
* Le gestionnaire d'énergie

_Remarques :_  
_DMA : Direct Memory Access : permet de transférer directement les données vers ou depuis un périphérique , sans intervention du CPU_  
_FPU unité de calcul pour les nombres flottants simple ou double précision_  
_Machine Learning : circuit dédié aux opérations sur les matrices utilisé par exemple dans les applications d'intelligence artificielle._

## Quels sont les avantages d'un  SoC ?

* Une augmentation de la vitesse de communication des éléments de l'ordinateur résultant : la diminution des distances permet de gagner en rapidité car plus la distance augmente, plus les hautes fréquences ont tendance à provoquer des pertes d'informations
* La diminution de la taille des composants signifie également une baisse de la consommation globale du système
* La diminution de la consommation signifie également une diminution de l'échauffement et donc aucune nécessité d'équiper le système d'un ventilateur : un système sur puce est donc silencieux !  

## Architecture  

![](./img/archi_SoC_PC.png)  

L’architecture ARM est la plus courante sur les SoC car n’importe quel fabricant peut concevoir et fabriquer des puces ARM en achetant une licence à la société éponyme.
Les instructions RISC, plus courtes et plus simples, sont censées s’exécuter plus vite sur le processeur.

Cela permet donc de créer des systèmes informatiques rapides et encore moins gourmand en énergie (à capacité comparable) qu'un microprocesseur ou qu'un microcontrôleur.

Au delà de l'informatique embarquée, on trouve maintenant des Soc sur de vrais systèmes informatiques, pouvant jouer ou non le rôle de "vrais" ordinateurs

## Composants  

Fonctionnement d’un système sur puce (SoC) à partir de composants
constitués de composants, eux mêmes réalisés à partir de portes logiques et
de transistors :
https://www.youtube.com/watch?v=NKfW8ijmRQ4  

Voici le diagramme d’un SoC de smartphone ainsi qu’une descriptions des composants couramment rencontrés :  

![smartphone](./img/snapdragon.jpg)  

|Nom	|Rôle|
|:---:|:---|
|CPU |Central Processing Unit : c’est le processeur et chef d’orchestre du SoC comme sur un PC. Il peut être composés de plusieurs cœurs et travaille à une certaine fréquence|
|GPU	|Graphics Processing Unit : en charge de calculer les images affichées à l’écran|
|ISP|	Image Signal Processor : gère les images prises par l’appareil photo
|DSP	|Digital Signal Processor : gère les signaux en provenance du micro, des accéléromètres, GPS...|
|Display|	Gère l’écran en lien avec le GPU|
|NPU	|Neural Processing Unit : gère tout ce qui est en lien avec le machine learning (reconnaissance vocale, habitudes...)|
|NoC	|Gère la communication entre tous les composants|
|Interface Modem|	Interface de communication vers modem 3G/4G/5G, WiFi, Bluetooth...|
|SPU	|Security Processing Unit : gère le cryptage/décryptage des données
|Memory	|Gère les transferts de données entre CPU et mémoire cache ou mémoire DRAM|
|Video|	Gère le codage/décodage des flux vidéo (MP4)|
|Audio	|Gère le codage/décodage des flux audio (MP3)|
|Storage|	Gère les transferts de données avec la mémoire Flask et/ou la carte SD|
|GPIO|	General Purpose Input Output : entrées/sorties vers boutons, leds|

* Unités utilisées pour comparer les puissances de calcul :
    * le nombre de transistors
    * le nombre d’instructions exécutées à la secondes (MIPS : Million of Instructions Per Second, GIPS ou TIPS). Souvent utilisé pour les CPU
    * le nombre de calculs effectués par seconde (FLOPS : Floating-point Operations Per Second). Souvent utilisés pour les GPU
    * les benchmarks

\newpage
Les mémoires peuvent être de différents types :  
![memoire](./img/memoire.png)  

Sources :  
* NSI Terminale Ellipses T. Balabonski,S. Conchon, JC. Filliatre, K Nguyen  

* Cours NSI P Lucas Lycée Pasteur Hénin Beaumont

* https://www.infoforall.fr/act/archi/systeme-sur-puce-soc/

* Cours NSI S Ramstein Lycée Quenot à Villeneuve d'Ascq 

* cours système sur puce Lycée Saint André Niort A Marot D Sallé J Simonneau

___
