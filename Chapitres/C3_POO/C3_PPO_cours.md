---
title: "Structure de données : 3_Programmation orientée objet"
subtitle: "cours"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

![BO](./img/BO.jpg)

__Problématique__ : Comment modéliser des éléments du monde réel sous forme d'objets informatiques afin de les manipuler de manière simple?

# Historique  

La __P__ rogrammation __O__ rientée __O__ bjet  trouve son origine en Norvège au début des années 1960. Elle fut élaborée par les informaticiens Ole-Johan Dahl et Kristen Nygaard.  
Dans les années 1970, elle fut reprise par l'informaticien américain Alan Kay qui contribua à son essor. Cependant, il fallut attendre les années 1990 pour que ses principes soient clairement formalisés.  

La POO est un __paradigme de programmation__ qui consiste à considérer un programme comme un ensemble d'objets en interaction : ceux-ci peuvent représenter un concept du monde réel comme un livre, meuble, une personne...  

# Les classes sous Python  

## Introduction

Nous allons considérer un objet `Individu` qui représente une personne.  
Cette personne a plusieurs caractéristiques :  

* sa date de naissance
* son genre
* sa taille
* sa masse
* etc... qui vont représenter les _variables_ de la personne.

De plus, cette personne peut faire plusieurs choses :  

* parler
* manger
* marcher  
* etc...  qui vont correspondre à des _fonctions_ propres à elle-même.

Pour définir cette personne ainsi que les fonctions qui lui sont associées, on peut définir une __classe.__  

## Définitions et vocabulaire  

>__📢 Définition 1 :__
> Une __classe__ est un entité informatique qui définit en son sein :  
>* des variables membres ( appelées __attributs__) ;
>* des fonctions membres (appelées __méthodes__) ;  

| Nom de la classe       |
|------------------------|
| + attribut : type      |
| + méthode(type) : type |

__Exemple :__ 

```python
class Personne :
    def __init__(self , birthyear , tall , weight) :
        self.birthyear = birthyear
        self.tall = tall
        self.weight = weight
        
    def grandir(self , h) :
        self.tall = self.tall + h
        
luc = Personne(2002 , 178 , 69)
init_tall = luc.tall
luc.grandir(8)
new_tall = luc.tall

print("Luc pèse ", luc.weight , "kg.")
print("Luc mesure à présent" , new_tall , "cm après avoir grandi de 8 cm en 1 an. Avant ça, il mesurait : " , init_tall, "cm")
```

![tutor](./img/instance.jpg)

```python
Luc pèse 69 kg.
Luc mesure à présent 186 cm après avoir grandi de 8 cm en 1 an. Avant ça, il mesurait :  178 cm
```

\newpage
>__📢 Définition 2 :__  
>Dans l'exemple précédent,  
>* _birthyear_, _tall_ et _weight_ sont des __attributs__ de la classe `Personne` ;
>* _grandir_ est appelée une  __méthode__ de la classe `Personne` ;  

On accède aux attributs d'une classe à l'aide de la syntaxe :  
\<nom de la classe>.<nom de l'attribut> 

On accède aux méthodes d'une classe à l'aide de la syntaxe :  
\<nom classe>.<nom méthode>(\<arguments potentiels>)  

# Constructeur d'une classe  

## Définition

Par convention, les classes doivent être nommées en CamelCase : une suite de mots, sans espace ni séparateur, dont la première lettre est une capitale.

>__📢 Définition 3 :__  
> Le __constructeur__ d'une classe est une _fonction_ :  
>* dont le nom est nécessairement _ _ __init__ _ _ ;  
>* admettant au moins un paramètre nommé __self__ placé obligatoirement en premier s'il y en a plusieurs.

![instance](./img/instance.jpg)  
![instance](./img/instance2.jpg)  

Le paramètre _self_ représente l'objet cible : c'est une variable qui contient une référence vers l'objet qui est en cours de création. Grâce à ce dernier, on va pouvoir accéder aux attributs et fonctionnalités de l'objet cible.  

## Instanciation et variables d'instance  

Une classe est une sorte de moule qui sert à fabriquer des objets.  
L'__instanciation__ est l'opération qui consiste à créer un objet. L'objet ainsi créé est alors appelé une _instance_ de la classe.  
Ainsi dans notre exemple `luc` est une instance de la classe `Personne`. 

```python
luc = Personne(2002 , 178 , 69)
```

Si on affiche la variable `luc`, on obtient :  

```python
>>> luc
<__main__.Personne object at 0x0442F178>
```

Cela signifie que luc est une référence vers l'objet créé, c'est à dire une indication de son emplacement mémoire. (ici 0x0429F178)  

Un attribut de la classe est représenté par une variable appelée _variable d'instance_, accessible à l'aide du paramètre _self_. Le constructeur initialise ces variables d'instances, qui seront alors stockées dans l'objet et en mémoire pour toute la durée de vie de l'objet.  

ATTENTION  __Faire la différence entre les deux types de variables__

* la variable `self.tall` représente la variable d'instance, c'est à dire celle associée à l'objet, qui existe à partir de la création de l'objet jusque sa destruction ; 
* la variable `tall` représente le paramètre reçu par le constructeur et n'existe que dans le corps de ce dernier.  

__Le paramètre `self` permet donc d'accéder aux variables d'instance, c'est à dire aux attributs de l'objet cible, depuis le constructeur.__  

# Méthode d'une classe  

## Définition  

__📢 Définition 4 :__  
> Une __méthode__ d'une classe est une fonction définie au sein de la classe et admettant au moins le paramètre `self` (obligatoirement en premier s'il y a plusieurs paramètres).  

On distingue :  

* des méthodes spéciales : leurs noms, réservés par le langage Python, commencent et finissent par « __ » : deux tirets « bas »
* des méthodes personnalisées
* des méthodes privées (inaccessibles en dehors des méthodes de l’objet lui-même) : leurs noms commencent par « __ » : deux tirets « bas »

![instance](./img/methode.jpg)

Le paramètre `self` représente l'objet cible sur lequel la méthode est appelée. Il permet notamment d'avoir accès aux variables d'instance de l'objet.  

Dans l'exemple précédent, _grandir(self,h)_ est une méthode admettant deux paramètres, dont le second est un nombre. Cette méthode redéfinit la variable d'instance _self.tall_ en lui ajoutant la valeur passée en second paramètre.  

![instance](./img/fonction_fin.jpg)

[lien vers l'éxécution dans Pythor tutor](https://urlz.fr/g8uS)

## Méthodes particulières en Python  

Ces méthodes particulières sont entourées de _ _ comme pour `init`.

|méthode|appel|effet|
|:---|:---:|:---|
|__ __str____ (self) |__str__(t) | renvoie une chaîne de caractères décrivant __t__ |
|__ __lt____ (self,u) |t < u | renvoie _True_ si t est strictement plus petit que _u_ |
|__ __hash____ (self) |__hash__(t) | donne un code de hachage pour _t_, par exemple pour l'utiliser comme clé d'un dictionnaire _d_ |

D'autres sont spécifiques à des classes représentant certaines catégories d'objets. Ci-dessous trois méthodes pour une classe représentant une collection.  

|méthode|appel|effet|
|:---|:---:|:---|
|__ __len____ (self) |__len__(t) | renvoie un nombre entier déffinissant la taille de __t__ |
|__ __contains____ (self,x) |x __in__ t| renvoie _True_ si et seulement si x est dans la collection t |
|__ __getitem____ (self,i) |__t[i]__| renvoie le -__i__-ème élément de __t__ |
\newpage

# Encapsulation

## Opérations sur les objets

On souhaite définir , par exemple , un objet `Vector` représentant un vecteur du plan, il serait utile de pouvoir définir la somme de deux de ces vecteurs, qui est aussi un objet de classe `Vector`:

```python
class Vector:
    def __init__(self,x,y):
        self.x = x
        self.y = y
    
    def __add__(self,other):
        return Vector(self.x + other.x, self.y + other.y)
```

```python
>>> u , v = Vector(-2,8) , Vector(3,-5)
>>> w = u + v  # w sera une instance de classe vector
>>> print("w({},{})".format(w.x,w.y))
w(1,3)
```

## Encapsulation

La classe `Vector` définie précédemment aurait pu être définie autrement :

```python
class vector :
    def __init__(self,x,y):
        self.coord = (x , y)
    
    def __add__(self , other) :
        return vector(self.coord[0] + other.coord[0] , self.coord[1] + other.coord[1])
```

On est passé de deux variables d'instance à une seule, mais cette différence ne doit pas être perçue par l'utilisateur.rice : la manière dont on gère les valeurs passées en paramètres au sein de la classe ne doit pas être vue " de l'extérieur".

__Le fait de réunir dans un même objet les attributs et les méthodes se nomme l’encapsulation.__

Sources :  

* NSI Terminale Nathan les vrais exos S Pasquet M Leopoldoff
* NSI Terminale Ellipses T. Balabonski,S. Conchon, JC. Filliatre, K Nguyen
* Cours NSI C Faury Lycée Blaise Pascal à Clermond Ferrand
