---
title: "Chapitre 3 POO"
subtitle : "Le jeu de la vie"
papersize: a4
geometry: margin=1.5cm
fontsize: 11pt
lang: fr
---

# Le sujet

Le jeu de la vie a été inventé par le mathématicien britannique John H. Conway (1937-2020). C'est un exemple de ce qu'on appelle un __automate cellulaire__. Il se déroule sur un tableau rectangulaire( L x H) de cellules. Une cellule est représentée par ses coordonnées x et y qui vérifient 0 $`\leq`$ x < L et 0 $`\leq`$ y < H.  
Une cellule peut être dans deux états : __vivante__ ou __morte__. La dynamique du jeu s'exprime par les règles de transition suivantes :  

* une cellule vivante reste vivante si elle est entourée de 2 ou 3 voisins vivantes et meurt sinon ; 
* une cellule morte devient vivante si elle possède exactement 3 voisines vivantes.  

La notion de "voisinage" dans le jeu de la vie est celle des 8 cases qui peuvent entourer une case donnée ( on parle de voisinage de Moore)

# Modélisation objet

1. Quelles classes peut-on dégager de ce problème au premier abord ?  

```





```

2. Quelles sont quelques-unes des méthodes qu'on pourrait leur donner ?

```





```

3. Dans quelle classe pouvons-nous représenter simplement la notion de voisinage d'une cellule ? Et le calculer ?  

```





```

4. Une cellule est au bord si x = 0, x = L-1, y = 0 ou y = H-1. Combien de voisins possède une cellule qui n'est pas au bord? Combien de voisins possède une cellule qui est au bord ?

```





```
\newpage
5. Que pourrions nous aussi considérer comme voisin de droite de la case en haut à droite de la grille ? Et comme voisin du haut ?

```





```

# Implémentation des cellules  

[Télécharger le fichier Python à compléter](jeu_de_la_vie_el.py)
## Class Cellule  
1. Implémenter tout d'abord une classe Cellule avec comme __attributs__ :  

* un booléen `actuel` initialisé à False  
* un booléen `futur` initialisé à False
* une liste `voisins` initialisée à None.  

Ces attributs seront ici considérés comme "privés". 
La valeur "False" signifie que la cellule est morte et "True" qu'elle est vivante.  

2. Ajouter les méthodes suivantes :  

* __est_vivant()__ qui renvoie l'état actuel ( vrai ou faux)  
* __set_voisins()__ qui permet d'affecter comme voisins la liste passée en paramètre ; 
* __get_voisins()__ qui renvoie la liste des voisins de la cellule  
* __naitre()__ qui met l'état futur de la cellule à True  
* __mourir()__ qui permet l'opération l'opération inverse  
* __basculer()__ qui fait passer l'état futur de la cellule dans l'état actuel.  

3. Ajouter à la classe `Cellule` une méthode __ __str____( ) qui renvoie une croix ( un X) si la cellule est vivante et un tiret (-) sinon.  
Expliquer brièvement l'utilité d'une telle méthode en Python :  

```


```

4. Ajouter une méthode __nb_vivants_voisins()__ qui calcule le nombre de voisins vivants dans la liste des voisins. Commenter et expliquer __calcule_etat_futur()__ :  

```










```

# La classe Grille  

1. Créer la classe Grille et y placer les attributs suivants considérés comme "publics" :  

* largeur ;  
* hauteur  ;  
* matrix : un tableau de cellules à 2 dimensions ( implémenté en Python par une liste de listes).  

Fournir une méthode __ __init__ __ permettant l'initialisation d'une Grille de Cellules avec une largeur et une hauteur ( une nouvelle Cellule sera créée par l'appel Cellule()).

2. Commenter les méthodes suivantes :

__dans_grille()__  

```




```

__setXY()__

```






```

__getXY()__

```






```

3. Compléter les méthodes get_largeur() et get_hauteur() dans le fichier Python qui récupèrent la largeur et la hauteur de la grille.

4. La méthode __est_voisin()__ est une méthode statique qui vérifie si les cases (i,j) et (x,y) sont voisines dans la grille.  Justifier.

```




```
5. Commenter, expliquer chacune des dernières méthodes de la classe Grille puis lancez le jeu !
