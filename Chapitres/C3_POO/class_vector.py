# class vector:
#     def __init__(self,x,y):
#         self.x = x
#         self.y = y
#     
#     def __add__(self,other):
#         return vector(self.x + other.x, self.y + other.y)
# 
# u , v = vector(-2,8) , vector(3,-5)
# w = u + v
# print("w({},{})".format(w.x,w.y))

class vector :
    def __init__(self,x,y):
        self.coord = (x , y)
    
    def __add__(self , other) :
        return vector(self.coord[0] + other.coord[0] , self.coord[1] + other.coord[1])
