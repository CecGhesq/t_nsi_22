---
title: "Structure de données : 1_Modularité Interface"
subtitle: "TP Interface programmation modulaire __Conception d’un module : module de gestion de données de type « heure »__"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---
On désire réaliser un module permettant de définir un type de données « heure » sur lequel des opérations classiques pourront être réalisées (addition, soustraction, comparaison, etc.)

Téléchargez le dossier « nsi_time » disponible [ici](./nsi_time.zip). Dans ce dossier, deux fichiers « nsi_time.py » et « time_1.py », repérez le module.

|![](./img/nsi_time.jpg)

# Constructeur d’une donnée de type heure

La fonction  `create()`  va nous permettre de définir un objet heure sous la forme d’un dictionnaire à partir d’une chaîne de caractères.
1. Tester en console :

```python
chaine = "essai de split".split(" ")
>>> chaine
```

Que fait la méthode split() ? Expliquer alors la ligne 

```python
hms=hour.split(':')
```

```



```

2. Observez le code et tenter de comprendre la démarche.  
Quels sont les rôles de `try` et `except` ?  

```



```

3. Dans le programme principal, importer le module  `time_1`  puis créez un objet heure « t1 ». Affichez ` t1`  dans la console et vérifiez que le type de  `t1`  est bien celui d’un dictionnaire.  

4. Décommentez puis complétez alors la « doctest »  de cette fonction dans le module `time_1.py` en y faisant apparaître tous les cas de fonctionnement (chaîne non conforme, nombre d’heure supérieur à 24 par exemple, etc.).  
Attention : ne pas utiliser la fonction print qui est ici redéfinie plus bas dans le constructeur.  

# Sélecteurs

Les sélecteurs de notre structure `heure` permettent d’obtenir le nombre d’heures, de minutes ou de secondes.
Retrouvez dans le module les méthodes permettant d’obtenir le nombre d’heures, de minutes ou de secondes.
Décommentez puis complétez la doctest de chacune de ces méthodes.  

# Comparaison de deux heures

En suivant la spécification fournie pour la fonction  `comp_time`  du module « time_1 », codez celle-ci puis vérifiez dans le programme principal  dans `nsi.py` le résultat attendu par la comparaison de 12:00:00 et de 5:45:12.

```python
import time_1 as time

# Programme principal
if __name__=='__main__':
    import doctest
    doctest.testmod(verbose=False)
    
    # Création d'une variable de type heure
    t1 = time.create("12:00:00")
    print("t1 :",t1)
    # Comparaison de deux varialbles de type heure
    t2 = time.create("5:45:12")
    if time.comp_time(t1, t2) == 0 :
        print (t1, "=" ,t2)
    elif time.comp_time(t1, t2) == 1 :
        print (t1, ">" ,t2)
    else :
        print(t1, "<" ,t2)
```

# Conversion d’un nombre de secondes en une structure heure

La fonction `seconds_hour()` permet l’obtention d’une heure en fonction du nombre de secondes passé en paramètre. Complétez la « doctest » de cette fonction ne faisant apparaître les différents cas que doit gérer celle-ci.

# Une autre réalisation du module

La première possibilité pour représenter une heure est l’utilisation de dictionnaire « {'h':h, 'm':m, 's':s} », une autre possibilité est d’utiliser des tuples « (hh, mm, ss) ».
Il est facile de réécrire le constructeur de ce module.  

1. Éditez le module  `time_2.py` . A partir du code des sélecteurs du module  `time_1.py` , réalisez celui des sélecteurs du module 2.

2. Copiez dans le module `time_2.py` les fonctions permettant la soustraction et la comparaison de deux objets heure. Dans le programme principal, sélectionnez ce module dans la ligne d’importation du module. Vérifiez alors que sans rien modifier (sauf le nom du fichier du module précédent), la seconde implémentation produit le même fonctionnement qu’avec le module N°1.

3. Vérifiez à l’aide de la fonction « help() » l’interface publique de ce module.

# Pour aller plus loin…. Addition, Soustraction de deux heures

1. Testez, à partir du programme principal la fonction `sub_time()`.

2. En déduire le code de la fonction `add_time()` permettant la somme de deux objets heure. Si celle-ci dépasse 24h, on redémarre à 0. Par exemple 13:00:00 + 12:00:00 donne 1:00:00.  

Source :  
* TP Interface de Pascal Lucas lycée Pasteur Hénin Beaumont
