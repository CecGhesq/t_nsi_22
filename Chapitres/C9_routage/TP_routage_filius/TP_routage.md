---
title: "Architecture matérielle : 9 Protocole de routage"
subtitle: "TP routage statique"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

Le but de cette activité est de compléter les tables de routage du réseau suivant afin de relier le pc au serveur :  

![reseau](./img/reseau.png)  

📌 A faire :

1. Ouvrir le fichier [routage.fls](ROUTAGE.fls) dans le logiciel Filius.
2. En mode conception ![](./img/mc.png) double-cliquez sur l'onglet "Général" des routeurs puis "Gérér les connexions" : on obtient ainsi les IP de chacune des cartes réseaux et les liaisons.
3. Complétez le schéma ci-dessus avec les adresses IP en notation CIDR de chacune des interfaces (cliquer sur les onglets de chacune des connexions pour obtenir les __valeurs de masques de sous réseaux__) : 
* /24 en notation CIDR correspond à 255.255.255.0
* /30 en notation CIDR correspond à 255.255.255.252  
Par exemple :  
![](./img/R4.png)

Le PC et le serveur doivent être reliés à un routeur pour avoir accès au réseau :  
📌 A faire :  

4. Donner l'adresse de l'interface réseau pour le pc puis pour le serveur en remplissant le champ "Passerelle" des machines  avec l'adresse du routeur ( exemple 192.168.0.2 pour le PC)

Afin de relier le pc au serveur, il va falloir __compléter les tables de routage__ une par une.  

📌 A faire :  

5. Choisir en mode conception le __routeur R1__ puis observer dans la table de routage la ligne :  
192.168.0.0	/255.255.255.0/	192.168.0.2	/192.168.0.2  
A quoi sert-elle ?

```

```

Le routeur R1 est relié à R3  : le réseau s'appelle 10.1.1.0/30 vers 10.1.1.2 en sortant par l'interface 10.1.1.1 : la table R1 ci-dessous est complétée (la métrique n'est pas dans le logiciel Filius); 

__R1__
|Réseau destination|masque|Passerelle|Interface|Métrique|
|:---:|:---:|:---:|:---:|:---:|
|10.1.1.0|255.255.255.252|10.1.1.2|10.1.1.1|1|

📌 A faire :  

6. Compléter la table de routage de R1 dans Filius en ajoutant une ligne dans le mode conception.

7. Compléter toutes les tables ci-dessous avec les liens entre chaque routeur :

__R2__
|Réseau destination|masque|Passerelle|Interface|Métrique|
|:---:|:---:|:---:|:---:|:---:|
|10.1.2.0|255.255.255.252|10.1.2.1|10.1.2.2|1|
|10.1.......|||||

__R3__
|Réseau destination|masque|Passerelle|Interface|Métrique|
|:---:|:---:|:---:|:---:|:---:|
|10.1.......|||||
|10.1.......|||||
|10.1.......|||||
|10.1.......|||||

__R4__
|Réseau destination|masque|Passerelle|Interface|Métrique|
|:---:|:---:|:---:|:---:|:---:|
|10.1.......|||||
|10.1.......|||||

__R5__
|Réseau destination|masque|Passerelle|Interface|Métrique|
|:---:|:---:|:---:|:---:|:---:|
|10.1.......|||||
|10.1.......|||||
|10.1.......|||||
|10.1.......|||||

__R6__
|Réseau destination|masque|Passerelle|Interface|Métrique|
|:---:|:---:|:---:|:---:|:---:|
|10.1.......|||||

8. Remplir les tables de routage dans Filius pour tous les routeurs

Pour finaliser le travail, il faut désormais donner la route entre le pc et le serveur.  

![reseau](./img/reseau.png)  
Pour réaliser la liaison vers le réseau 192.168.6.0 depuis R1 : il faut mettre la référence vers ce réseau dans les routeurs qui seront traversés : par exemple R1/R3/R5/R6  

📌 A faire :  

9. Compléter le __tableau__ ci-dessous puis les __lignes__ dans les tables de routage.

|Routeur|Réseau destination|masque|Passerelle|Interface|Métrique|
|:---:|:---:|:---:|:---:|:---:|:---:|
|R1|192.168.6.0|255.255.255.0|10.1.1.2|10.1.1.1|3|
|R3|192.168.6.0|255.255.255.0|10.1....|10.1....|.|
|R5|192.168.6.0|255.255.255.0|10.1....|10.1....|.|

10. En mode simulation ![](./img/ms.png) cliquer droit sur le pc et _"afficher le bureau"_. Lancer une commande  "ping 192.168.6.0" ; que se passe-t-il? 
```


```
11. Cliquer droit sur le pc et _"afficher les échanges de données"_ (192.168.0.10)". 

* Quel est le rôle du protocole ARP?

```


```

* Quel est le rôle du protocole ICMP?

```


```

* Pourquoi la commande ping n'a-t-elle pas aboutie ?

```


```

12. Pour réaliser le retour, on passera par les routeurs R6/R5/R4/R3/R1. Compléter le __tableau__ ci-dessous puis les __lignes__ dans les tables de routage.

|Routeur|Réseau destination|masque|Passerelle|Interface|Métrique|
|:---:|:---:|:---:|:---:|:---:|:---:|
|R6|192.168.0.0|255.255.255.0|10.1.7.1|10.1.7.2|.|
|R5|192.168.0.0|255.255.255.0|10.1. . . .|10.1. . . .|.|
|R4|192.168.0.0|255.255.255.0|10.1. . . .|10.1. . . .|.|
|R3|192.168.0.0|255.255.255.0|10.1. . . .|10.1. . . .|.|

13. Lancer à nouveau la commande _"ping 192.168.6.0"_ . Regarder à nouveau sur les machines "afficher les échanges de données"_

14. A l'aide de la commande _"traceroute 192.168._._"_ sur chaque machine déterminer le chemin emprunté et le surligner sur le schéma initial.

Nous allons dorénavant passer en routage automatique et etudier comment s'adapte le réseau en cas de modification du réseau.  

📌 A faire :  

15. En mode conception, cocher pour chaque routeur dans l'onglet général le __mode automatique__.

16. Enregistrer le fichier sous un autre nom pour différencier de la partie précédente.

17. Réaliser un "traceroute" à partir de chacune des deux machines et surligner sur le schéma ci-dessous la route empruntée.  

![reseau](./img/reseau.png) 

Pourquoi est-ce celle-là qui a été choisie ?
```

```

18. Supprimer le cable entre les routeurs R3 et R5. Relancer le traceroute sur chacune des machines. Etait-ce la seule possibilité de route? 

```

```
19. Relier les routeurs R3 et R6 à l'aide d'un câble. Réaliser un "traceroute" à partir d'une machine : que se passe-t-il? 

```

```
20. Afin d'établir la connexion entre R3 et R6 il faut donner des __adresses IP aux routeurs cohérentes__ avec le bon masque de sous-réseau. Modifiez celle-ci et vérifier la route empruntée par les paquets à l'aide de la commande "traceroute".
