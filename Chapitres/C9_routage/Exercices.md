---
title: "Architecture matérielle : 9_Protocoles de routage"
subtitle: "TP"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---


# Exercice 1 : table de routage  🏆 
![](./img/r1.jpg)  
1. Soit la __table de routage statique__ du Routeur_1 ( simplifiée)du réseau ci-dessus : 

|Destination|Passerelle|
|:---:|:---:|
|192.168.0.0/25|192.168.0.1|
|192.168.1.0/25|192.168.1.1|
|0.0.0.0/0|192.168.1.3|
|100.10.42.0/25|192.168.1.2|

La troisième ligne correspond au chemin par défaut vers internet : il doit se diriger vers le routeur 3.

Ecrire sur ce modèle les tables de routage statique de Routeur_2 et Routeur_3.  
Donner la table de routage de tous les routeurs du réseau proposé selon le protocole RIP.

2. Prévoir la réponse sur la machine __100.10.42.100__ en console de :

```
/>traceroute 92.168.4.100
```

Vérifier en ouvrant dans le logiciel Filus ce [fichier](reseauPB1.fls). 

3. Donner la __table de routage dynamique__ de tous les routeurs du réseau.
 Voici la situation avant que les routeurs n'écoutent leur voisins.

![](./img/res.jpg)

# Exercice 2

On se place dans le cas de 4 réseaux (R1 R2 R3 R4) et 3 routeurs (A B C)  placés en ligne et en utilisant le protocole RIP.  

![](./img/CYCLES.png)  

1. Donner les tables de routage __initiales__ simplifiées des 3 routeurs. 
2. Donner les tables successives après chaque période de 30 s jusqu'à ce que les 4 réseaux apparaissent pour la première fois dans les tables des trois routeurs avec des distances finies.
3. Le routeur C tombe en panne. Expliquer l'anomalie constatée et comment le fait que la distance infinie de 16 sauts permet de résoudre ce problème. Au bout de combien de temps le réseau R4 est-il considéré comme inaccessible pour les routeurs A et B?


# Exercice 3  🏆🏆
1. Considérons le réseau ci-dessous :  
![](./img/ex_reseau.png)

Dans ce réseau, les nœuds A à F sont des routeurs dont on veut calculer les tables de routage. On suppose que l'on a exécuté le protocole RIP sur ce réseau. Compléter la table suivante, qui indique pour chaque machine la portion de la table de routage pour la destination G. 

|machine| destination|passerelle|interface|distance|
|:---:|:---:|:---:|:---:|:---:|
|A|G||||
|B|G||||
|C|G||||
|D|G||||
|E|G||||
|F|G||||

2. On suppose que le lien __B-F__ tombe en panne.  
    a. Quel est le vecteur de distance envoyé par B à ses voisins pour atteindre G, une fois qu'il détecte la panne (on suppose que les autres 
    nœuds n'ont pas modifié leurs tables de routage)?  
    b. Pour chacun des événements suivants, dire lequel des quatre cas du protocole RIP (voir cours). On supposera, pour simplifier, qu'aucun autre événement ne se produit entre-temps et qu'ils sont tous exécutés « en séquence ».  
--> Les routeurs A et C reçoivent de B le vecteur trouvé à la question 2.  
--> Le routeur C retransmet ce même vecteur à D.  
--> Le routeur D transmet le vecteur (G,3) à C.  

3. Après le dernier cas ci-dessus, quel vecteur est transmis par C à A 
et B? 

4. On remet le réseau comme initialement. Pour chacun des liens du réseau, proposer une technologie réseau faisant que, pour les nœuds A, B et C, la route pour atteindre G soit différente selon que l'on utilise OSPF ou RIP.  


Sources :  
* NSI Terminale Ellipses T. Balabonski,S. Conchon, JC. Filliatre, K Nguyen  
* NSI Terminale Prépabac Hatier  
* NSI Terminale Nathan les vrais exos S Pasquet M Leopoldoff  
* Cours NSI S Ramstein Lycée Quenot à Villeneuve d'Ascq  
* Cours NSI A Wilm Lycée Beaupré à Haubourdin  
* NSI Terminale Ellipses JC Bonnefoy B Petit  
