---
title: "Architecture matérielle : 6 Processus_ressources"
subtitle: "TP 2 Multi-Thread Interblocage"
papersize: a4
geometry: margin=1.5cm
fontsize: 10pt
lang: fr
---

# Bug mystérieux

## Présentation

Ouvrir le script [compteur_v1.py](./script/compteur_v1.py):  

```python
# Librairie utilisées
from threading import Thread 

# Variable partagée par les 2 threads
compteur = 0

def incrementer(limite):
    """Incrémente le compteur"""
    global compteur
    for i in range(0, limite):
        compteur = compteur + 1

def decrementer(limite):
    """Décrémente le compteur"""
    global compteur
    for i in range(0, limite): 
        compteur = compteur - 1

if __name__ == '__main__':
    print(f"Compteur au début => {compteur}")
    # Création des threads
    t1 = Thread(target=incrementer, args=(1000000,))
    t2 = Thread(target=decrementer, args=(1000000,))

    # Lancement des threads
    t1.start()
    t2.start()

    # Attente de la fin du travail
    t1.join()
    t2.join()

    # Affichage du résultat
    print(f"Compteur à la fin => {compteur}")
```

Le script lance 2 threads en parallèle :
* un thread incrémente un compteur partagé 1 000 000 de fois
* un autre thread décrémente ce même compteur 1 000 000 de fois

Lancez ce script. Que constatez-vous ?

## Explications
Pour mieux comprendre l'origine du problème, il faut zoomer sur les opérations d'incrémentation et de décrémentation dans chacun des threads.

__Scénario idéal__:  
Voyons ce que cela donne en pseudo langage machine. Chaque ligne représente une instruction exécutée par le processeur. N est le compteur, R1 et R2 des registres de travail du processeur.

|Etapes| Thread 1 (incrémenter)| Thread 2 (décrémenter)|
|---|---|--|
|1|Charger N dans R1||
|2|Incrémenter R1 de 1||
|3|Ranger R1 dans N||
|4||Charger N dans R2|
|5||Décrémenter R2 de 1|
|6||Ranger R2 dans N|

Dans le cas de figure précédent tout se déroule correctement. N=0 en début de programme, il passe à 1 avec le travail de la thread 1 puis repasse à 0 avec le travail de la thread 2. A la fin N=0 comme prévu.

__Scénario problématique__:  
Imaginons maintenant la situation suivante, où la commutation de contexte entre la thread 1 et la thread 2 intervient en plein milieu du calcul, par exemple entre les étapes 2 et 3.  

|Etapes| Thread 1 (incrémenter)| Thread 2 (décrémenter)|
|---|---|--|
|1|Charger N dans R1||
|2|Incrémenter R1 de 1||
|3||Charger N dans R2|
|4||Décrémenter R2 de 1|
|5||Ranger R2 dans N|
|6|Ranger R1 dans N||

Au début N=0.  
Étape #2, l'incrémentation n'a pas le temps de se terminer qu'elle est coupée par les 3 étapes de décrémentation.   
Étape #5, N=-1.  
Étape #6, le contexte de la thread 1 est restauré, R1 était alors égal à 1, au final N=1  

Conclusion, sur un million de fois cette situation se produit de temps en temps comme en atteste l'exécution du programme.

## Une solution

Une solution possible consisterait à verrouiller les calculs dans chaque thread afin de ne pas être coupé en plein milieu et produire des erreurs : il faut protéger l’accès au compteur N.

|Etapes| Thread 1 (incrémenter)| Thread 2 (décrémenter)|
|---|---|--|
|1|VERROUILLER LE CALCUL||
|2|Charger N dans R1||
|3|Incrémenter R1 de 1||
|4|Ranger R1 dans N||
|5|DEVERROUILLER LE CALCUL||
|6||VERROUILLER LE CALCUL|
|7||Charger N dans R2|
|8||Décrémenter R2 de 1|
|9||Ranger R2 dans N|
|10||DEVERROUILLER LE CALCUL|

En Python, c’est tout à fait possible grâce à la classe Lock du module
threading.

```python
Librairie utilisées
from threading import Lock
# Création d'un verrou partagé par les 2 threads
verrou = Lock()
# Verrouillage
verrou.acquire()
##
Ici une section critique ne devant pas être coupée
#
# Déverrouillage
verrou.release()
```

Corrigez le script compteur_v1.py à l’aide des verrous présentés
précédemment.
Vérifiez que le compteur vaut bien 0 après l’exécution des 2 threads.

# Interblocage dans le robot  

A l’aide des classes `Thread` et `Lock` du module `threading`, écrire une simulation du robot présenté [ici](animation_interblocage_robot.pdf).

## Les processus

On représente les processus par 3 fonctions qui seront embarquées
chacune dans un Thread dédié :

* P1_pilotage_manuel()
* P2_envoi_flux_video()
* P3_auto_tests_materiels()  

Afin de simuler un travail pour chacun de ces 3 processus, on utilisera les fonctions `sleep()` et `random()`.

Vous vous assurerez que les temporisations avec sleep() sont courtes (~10ms)
afin de voir le phénomène d’interblocage survenir.

## Les ressources

Les ressources seront représentées par 3 objets de la classe Lock :
* R1_moteurs
* R2_wifi
* R3_camera

Afin de visualiser l’allocation des ressources par les processus, effectuez un affichage de type :  
Processus P2 : demande la ressource R2  
Processus P2 : utilise la ressource R2  
Processus P2 : libère la ressource R2...  

L’interblocage ?  
Vérifiez que la situation d’interblocage survient de manière aléatoire en
lançant votre programme plusieurs fois.

Sources :  
* cours Niort lycée Saint André A MAROT D SALLÉ J SIMONNEAU 
