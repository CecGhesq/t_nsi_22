# T_NSI_22

Dépôt de fichiers de __terminale NSI__ Lycée M de Flandre à Gondecourt (C. Ghesquiere)

* Adresse mail  : cecile.ghesquiere@ac-lille.fr 
* [Lien vers le réseau du lycée](https://194.167.100.29:8443/owncloud/index.php) : mettre vos identifiants comme au lycée



<table class="tftable" border="1">
<tr><th>chapitre</th><th>programme</th><th>déroulé</th></tr>

<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-007.gif"></td>
<td style="color:#e06666" > algorithmique </span></td>
<td> <b> Recherche Textuelle</b> </br>
<ul>
    <li>Cours  <a href ="./Chapitres/C16_recherche_textuelle/C16_cours/C16_cours.md"> md </a> et  <a href ="./Chapitres/C16_recherche_textuelle/C16_cours/algorithme_Boyer_Moore.pdf"> diaporama </a> </li>
    <li> Exercices <a href ="./Chapitres/C16_recherche_textuelle/exos/C16_exos.md"> md </a></li>

</td>
</tr>

<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-006.gif"></td>
<td style="color:#e06666" > algorithmique </span></td>
<td> <b> Calculabilité</b> </br>
<ul>
 Cours  <a href ="./Chapitres/C15_Calculabilite/Cours_calculabilite.md"> md </a>  

</td>
</tr>

<td><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-005.gif"></td>
<td style="color:#8973c1" >Architecture matérielle</span></td>
<td>
 <b>Sécurité des communications </b> </br>
<ul><li> Cours  <a href ="./Chapitres/C14_securite_comm/C14_securisation_communication.pdf"> diaporama </a>  </li>
<li> TP Signature et chiffrage d'un fichier :  <a href ="./Chapitres/C14_securite_comm/C14_TP_SHA.md"> md </a>   </li> 

<li> TP RSA : <a href = "././Chapitres/C14_securite_comm/RSA/C14_TP_RSA.md"> sujet </a> </li>
</td>
</tr>

<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-004.gif"></td>
<td style="color:#e06666" >Structure de données ; algorithmique </span></td>
<td> <b> Les graphes</b> </br>
<ul>
<li> Cours partie 1 <a href ="./Chapitres/C13_graphes/C13_1_cours.md"> md </a>  ou <a href ="./Chapitres/C13_graphes/C13_1_cours.pdf"> pdf </a>  </li>
<li> TP partie 1 :  <a href ="./Chapitres/C13_graphes/C13_1_TP_el.md"> md </a> ou <a href ="./Chapitres/C13_graphes/C13_1_TP_el.pdf"> pdf </a>  </li>
<li> Cours : partie 2  <a href ="./Chapitres/C13_graphes/C13_2_cours.md"> md </a>  ou <a href ="./Chapitres/C13_graphes/C13_2_cours.pdf"> pdf </a>  </li>
<li> Lien vers animations pour comprendre les parcours en profondeur et en largeur <a href ="http://fred.boissac.free.fr/AnimsJS/DariushGraphes/index.html"> lien (M Boissac) </a>
<li> Parcours de graphes  <a href ="./Chapitres/C13_graphes/Parcours_de_graphes.pdf"> pdf </a>  </li>
<li> Ours en cage : <a href ="./Chapitres/C13_graphes/ours_en_cage/ours_en_cage_el.md" > le sujet </a> et <a href ="./Chapitres/C13_graphes/ours_en_cage/ours_en_cage_el.zip" > le dossier à télécharger </li>
<li> Ice Walker <a href ="./Chapitres/C13_graphes/dossier_el_IW/projet_IW.md">présentation </a></li>
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/clavier/chiffres-gif-003.gif"> </td>
<td style="color:#8973c1" >Architecture matérielle</span></td>
<td><ul>
<li> <b>Microcontrôleur et SoC </b> <a href ="./Chapitres/C6_microcontroleur_SoC/C6_microcontroleur_SoCs.md"> md </a>  </li>
<li> TP  <a href ="./Chapitres/C6_microcontroleur_SoC/C6_TP_el.md"> md </a>  </li>
</td>
</tr>
<tr>
<td><img src="./img/35426.gif"> </td>

<td> DS 5 </td>
<td>  <a href = "./Evaluations/DS5"> Dossier </a> et les  <a href = "./Evaluations/DS5"> Corrigés </a> </td>
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-002.gif"></td>
<td style="color:#17657D" >Langages et  programmation </span></td>
<td> <b>Mise au point et optimisation </b> </br>
<ul>
<li> Cours  <a href ="./Chapitres/C11_mise_au point/cours/C11_mise_au_point.md"> md </a>  ou <a href ="./Chapitres/cours/C11_mise_au point.pdf"> pdf </a>  </li>
<li> TP :  <a href ="./Chapitres/C11_mise_au point/tp/C11_TP.md"> md </a> ou <a href ="./Chapitres/C11_mise_au point/tp/C11_TP.pdf"> pdf </a>  </li> 
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-002.gif"><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-001.gif"> </td>
<td style="color:#17657D" >Algorithmique</span></td>

<td> <b>Diviser pour régner </b> </br>
<ul>
<li> Cours  <a href ="./Chapitres/C10_div_reg/C10_diviser_regner.md"> md </a>  ou <a href ="./Chapitres/C10_div_reg/C10_diviser_regner.pdf"> pdf </a>  </li>
<li> TP :  <a href ="./Chapitres/C10_div_reg/C10_TP_div_reg.md"> md </a> ou <a href ="./Chapitres/C10_div_reg/C10_TP_div_reg.pdf"> pdf </a>  </li> 
<li>  <a href = "./Chapitres/C10_div_reg/exo_bac">Exercices de bac </a> </li>
</td>
</tr>
<tr>
<td><img src="./img/35419.gif"> </td>
<td> Bac blanc </td>
<td>  <a href = "./Evaluations/BB"> Dossier </a> et les  <a href = "./Evaluations/BB"> Corrigés </a> </td>
</td>
</tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-010.gif"></td>
<td style="color:#8973c1" >Architecture matérielle</span></td>
<td>
 <b>Routage   <img src="./img/noel_sa02.gif " style="width: 60px; height: auto;"></b> </br>
<ul><li> Cours  <a href ="./Chapitres/C9_routage/C9_protocole_RIP.md"> md </a>  ou <a href ="./Chapitres/C9_routage/C9_protocole_RIP.pdf"> pdf </a>  </li>
<li> Exercices :  <a href ="./Chapitres/C9_routage/Exercices.md"> md </a> ; vous pouvez télécharger Filius ensuivant ce [lien](https://ent2d.ac-bordeaux.fr/disciplines/sti-college/2019/09/25/filius-un-logiciel-de-simulation-de-reseau-simple-et-accessible/)</li>
<li> TP :  <a href ="./Chapitres/C9_routage/TP_routage_filius/TP_routage.md"> md </a> </li>
</td>
</tr>
<tr>
<td><img src="./img/35419.gif"> </td>
<td> DS 4 </td>
<td>  <a href = "./Evaluations/DS4"> Dossier </a> et les  <a href = "./Evaluations/DS4"> Corrigés </a> </td>
</td>
</tr>
<tr>

<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-009.gif"> </td>
<td style="color: #e06666">Structure de données</span></td>
<td><ul>
<li> <b>Pile File </b> <a href ="./Chapitres/C8_pile_file/C8_pile_file.md"> md </a>  ou <a href ="./Chapitres/C8_pile_file/C8_pile_file.pdf"> pdf </a></li>
<li> TP  <a href ="./Chapitres/C8_pile_file/C8_TP_eleve.md"> md </a>  ou <a href ="./Chapitres/C8_pile_file/exos/C8_TP_eleve.pdf"> pdf </a> </li>
<li style="color:#6aa84f"> sujet Bac 2021 Amérique du Nord  <a href ="./Chapitres/C8_pile_file/exos/sujet_bac_amerique2021_ex5.pdf"> : sujet </a>  </li>

<li> <img src="./img/ordi.gif" style="width: 60px; height: auto;"> EP : on s'entraîne sur les piles <a href="https://e-nsi.forge.aeif.fr/pratique/N2/770-file_avec_liste/sujet/"> ici </a> </li>
<li> <img src="./img/ordi.gif" style="width: 60px; height: auto;"> EP : on s'entraîne sur les files <a href="https://e-nsi.forge.aeif.fr/pratique/N2/770-filtre_pile/sujet/"> ici </a> </li>

</td>
</tr>

<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-008.gif"> </td>
<td style="color:#e06666" ><p>Structure de données</p>  <p> Algorithmique </p> </td>
<td><ul>
<li> <b>Les arbres </b>Cours partie 1 <a href ="./Chapitres/C7_arbres/cours/C7_1.md"> md </a>  
<li> TP 1 :  <a href ="./Chapitres/C7_arbres/TP/C7_TP1.md"> md </a> 
<li> Cours partie 2 <a href ="./Chapitres/C7_arbres/cours/C7_2_ABR.md"> md </a>  ou <a href ="./Chapitres/C7_arbres/cours/C7_2_ABR.pdf"> pdf </a>  </li>
<li> TP 2 :  <a href ="./Chapitres/C7_arbres/TP/C7_TP2.md"> md </a> ou <a href ="./Chapitres/C7_arbres/TP/C7_TP2_.pdf"> pdf </a>  </li>
<li> <img src="./img/ordi.gif" style="width: 60px; height: auto;"> EP : on s'entraîne sur les arbres <a href="https://e-nsi.forge.aeif.fr/pratique/N2/800-arbre_bin/sujet/"> ici </a> </li>
<li> <img src="./img/ordi.gif"> EP et sur les ABR <a href="https://e-nsi.forge.aeif.fr/pratique/N2/876-ABR/sujet/" > ici </a> </li>
</td>
</tr>

<td><img src="./img/35426.gif"> </td>
<td> DS 3</td>
<td>  <a href = "./Evaluations/DS3/DS3.pdf"> Sujet </a> et le  <a href = "./Evaluations/DS3/Correction_DS3 .pdf"> Corrigé </a> </td>
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-007.gif"> </td>
<td style="color:#8973c1" >Architecture matérielle</span></td>
<td><ul>
<li> <b>Processus Ressources </b> <a href ="./Chapitres/C6_processus/6_processus_ressource.md"> md </a>  ou <a href ="./Chapitres/C6_processus/6_processus_ressource_cours.pdf"> pdf </a></li>
<li> TP 1  <a href ="./Chapitres/C6_processus/TP/TP_C6_processus_def_el.md"> md </a>  ou <a href ="./Chapitres/C6_processus/TP/TP_C6_processus_def_el.pdf"> pdf </a> </li>
<li> TP 2  <a href ="./Chapitres/C6_processus/TP/TP2_C6_interblocage.md"> md </a>  ou <a href ="./Chapitres/C6_processus/TP/TP2_C6_interblocage.pdf"> pdf </a> </li>
<li >   <a href ="./Chapitres/C6_processus/metro_2021_ex2_2.pdf">sujet Bac 2021 sujet candidats libres </a> </li>
<li > <a href = "./Chapitres/C6_processus/2022_AS_J1_4.pdf"> sujet Amérique du Sud 2022 ex 4 </a>  </li>
 </td>
</tr>

<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-006.gif"> </td>
<td style="color: #e06666" >Structure de données</span></td>
<td><ul>
<li> <b>Liste chainée </b><a href ="./Chapitres/C5_liste_chainee/C5_liste_chainee.md"> md </a>  ou <a href ="./Chapitres/C5_liste_chainee/C5_liste_chainee.pdf"> pdf </a></li>
<li> TP  <a href ="./Chapitres/C5_liste_chainee/exos/C5_TP.md"> md </a>  ou <a href ="./Chapitres/C5_liste_chainee/exos/C5_TP.pdf"> pdf </a> </li>

</td>
</tr>

<td><img src="./img/35426.gif"> </td>
<td> DS 2</td>
<td>  <a href = "./Evaluations/DS2/DS2.pdf"> Sujet </a> et le  <a href = "./Evaluations/DS2/DS2_correction.pdf"> Corrigé </a> </td>
</td>
</tr>

<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-005.gif"> </td>
<td style="color:#09fa14" >Base de données</span></td>
<td>
 <b>Base de données relationnelles </b> </br>
<ul><li> Cours partie 1 <a href ="./Chapitres/C4_BDD/cours/C4_1.md"> md </a>  ou <a href ="./Chapitres/C4_BDD/cours/C4_1.pdf"> pdf </a>  </li>
<li> TP 1A :  <a href ="./Chapitres/C4_BDD/TP/C4_1A_TP.md"> md </a> ou <a href ="./Chapitres/C4_BDD/TP/C4_1A_TP.pdf"> pdf </a>  </li>
<li> TP 1B :  <a href ="./Chapitres/C4_BDD/TP/C4_1B_TP.md"> md </a> ou <a href ="./Chapitres/C4_BDD/TP/C4_1B_TP.pdf"> pdf </a>  </li>
<li> Cours partie 2 <a href ="./Chapitres/C4_BDD/cours/C4_2.md"> md </a>  ou <a href ="./Chapitres/C4_BDD/cours/C4_2.pdf"> pdf </a> <a href="./Chapitres/C4_BDD/cours/mementoSQL_LE_COUPANEC_JACQUES.pdf"> Memento SQL </a> </li>
<li> TP 2 :  <a href ="./Chapitres/C4_BDD/TP/C4_2_TP.md"> md </a> ou <a href ="./Chapitres/C4_BDD/TP/C4_2_TP.pdf"> pdf </a> </li>
<li> <a href = './Chapitres/C4_BDD/exos_bac'> Exercices de bac </a>
</td>
</tr>

<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-004.gif"> </td>
<td style="color: #e06666" >Structure de données</span></td>
<td><ul>
<li> <b>La programation orientée objet </b><a href ="./Chapitres/C3_POO/C3_PPO_cours.md"> md </a>  ou <a href ="./Chapitres/C3_PPO/C3_poo_cours.pdf"> pdf </a></li>
<li> TP POO <a href ="./Chapitres/C3_POO/tp/C3_TP_POO_el.md"> md </a>  ou <a href ="./Chapitres/C3_POO/tp/C3_poo_tp.pdf"> pdf </a> </li>
<li style="color:#6aa84f"> Sujet de bac <a href ="/Chapitres/C3_POO/Bac_Sujet2_Exercice_1.pdf"> pdf </a> </li>
<li > Jeu de la vie <a href ="./Chapitres/C3_POO/tp/jeu_de_la_vie/jeu_de_la_vie_el.py">programme à compléter </a> <a href ="./Chapitres/C3_POO/tp/jeu_de_la_vie/C3_TD_jeu_de_la_vie.md">sujet </a></li>


</td>
</tr>
<tr>
<td><img src="./img/35419.gif"> </td>
<td> DS 1 </td>
<td>  <a href = "./Evaluations/DS1/DS1_module_recurs.pdf"> Sujet </a> et le  <a href = "./Evaluations/DS1/DS1_correction.pdf"> Corrigé </a> </td>
</td>
</tr>

<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-003.gif"> </td>
<td style="color:#17657D" >Langages, programmation et algorithmique</span></td>
<td><ul>
<li><b>La récursivité </b><a href ="./Chapitres/C2/C2_Recursivite_cours.md"> md </a> </li>
<li> notions mathématiques <a href ="./Chapitres/C2/tp/2_recurrence_math_el.md"> md </a>  </li>
<li> TP Récursivité <a href ="./Chapitres/C2/tp/2_TP_recursivite_el.md"> md </a>  </li>
<li> TP Notion de mathématiques <a href ="./Chapitres/C2/tp/2_recurrence_math_el.md"> md </a>  </li>
<li>  <a href = "./Chapitres/C2/Ex2_zero_2021.pdf"> Sujet de bac 0 2021 </a> </li>
<li>  <a href = "./Chapitres/C2/polynesie_2022_ex1.pdf"> Exercice 1 Polynésie 2022 </a> </li>
</td>
</tr>
<tr>
<td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-002.gif"> </td>
<td style="color:#17657D" >Langages, programmation et algorithmique</span></td>
<td><ul>
<li> <b>Modularité Interface </b><a href ="./Chapitres/C1/C1_modularite.md"> md </a> </li>
<li> TP 1 modules<a href ="./Chapitres/C1/TP/TP_modularité_eleve.md"> md </a>  </li>
<li> TP 2 interface <a href ="./Chapitres/C1/TP/TP2_interface/C1_TP_interface.md"> md </a>  </li>
</td>
</tr>

<tr><td><img src="https://www.icone-gif.com/gif/chiffres/satellite/chiffres-gif-001.gif"></td><td>révision</td><td>boucle bornée, non bornée, comparaisons, listes, tuples, dictionnaires,.....
<a href ="./Chapitres/revisions/revisions.md"> lien vers la feuille de travail </a>  </a> 
</td>
</tr>

<tr><td>1_NSI</td><td></td><td><a href="https://framagit.org/CecGhesq/1_nsi_21"> git_premiere_nsi </a></td></tr>
</table>


<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />Les  documents sont mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Licence Creative Commons Attribution 4.0 International</a>.
